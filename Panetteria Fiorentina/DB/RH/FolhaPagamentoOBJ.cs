﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.RH
{
    class FolhaPagamentoOBJ
    {
        public decimal SalarioHora(decimal salario)
        {

            decimal salarioH = salario / 220;

            return salarioH;
        }

        public decimal ValeTransporte(decimal salario)
        {

            decimal Vale = (salario * 6)/100;

            return Vale;
        }

        public decimal CalculoINSS(decimal salario, decimal atrasos, decimal hrExtra, decimal dsr)
        {
            
            

            decimal INSS = (salario + hrExtra + dsr - atrasos);



            if (INSS <= 1693.72m)
            {

                INSS = (INSS * 8) / 100;

            }
            if (INSS >= 1693.73m && INSS <= 2822.90m)
            {

                INSS = (INSS * 9) / 100;


            }
            if (INSS >= 2822.91m && INSS <= 5645.80m)
            {

                INSS = (INSS * 11) / 100;


            }
            return INSS;

        }

        public decimal CalculoFGTS(decimal salario, decimal extra50, decimal extra100)
        {

            decimal FGTS = ((salario + extra50 + extra100) * 8) / 100;

            return FGTS;
        }

        public decimal HoraExtra50(decimal salario, int horas)
        {
            decimal salarioH = (salario / 220);

            decimal hora50 = (salarioH * 50) / 100;

            decimal salario50 = (hora50 + salarioH) * horas;



            return salario50;


        }
        public decimal HoraExtra100(decimal salario, int horas)
        {
            decimal horas100 = SalarioHora(salario);
            decimal hora100 = (horas100 * 2) * horas;
            
            


            return hora100;
        }

        public decimal CalculoImpostoRenda(decimal salario, decimal atrasos, decimal hrExtra, decimal dsr,decimal INSS)
        {
            decimal SalarioBase = salario + hrExtra + dsr - (atrasos + INSS);

            decimal ImpostoRenda = 0;

            decimal Imposto = 0;

            if (SalarioBase <= 1903.98m)
            {

                Imposto = 0.00m;

            }
            if (SalarioBase >= 1903.99m && SalarioBase <= 2826.65m)
            {
                ImpostoRenda = (salario * 7.5m) / 100;

                if (ImpostoRenda >= 142.80m)
                {

                     Imposto =  142.80m;

                }


            }
            if (SalarioBase >= 2826.66m && SalarioBase <= 3751.05m)
            {

                ImpostoRenda = (salario * 15) / 100;

                if (ImpostoRenda >= 354.80m)
                {

                     Imposto = 354.80m;

                }


            }
            if (SalarioBase >= 3751.06m && SalarioBase <= 4664.68m)
            {

                ImpostoRenda = (salario * 22.5m) / 100;

                if (ImpostoRenda >= 636.13m)
                {

                     Imposto = 636.13m;

                }

            }
            if (SalarioBase >= 4664.68m)
            {

                ImpostoRenda = (salario * 27.5m) / 100;

                if (ImpostoRenda >= 869.36m)
                {

                     Imposto = 869.36m;

                }


            }
            return Imposto;

            




        }
        public decimal CalcularAtrasos(decimal salariohora, int horas)
        {
            decimal valor = salariohora * horas;

            return valor;
        }

        public decimal CalcularDSR(decimal valorExtra, decimal domingos, decimal faltas)
        {


            decimal falta = 26 - faltas;            
            decimal dsr = (valorExtra / falta) * (4 - domingos);

            return dsr;
        }

        public decimal CalcularFalta(decimal salario, int falta, int domingo)
        {
            decimal faltas = (salario / 30) * falta;
            decimal faltas3 = (salario / 30);
            decimal resultado = 0;
            if (domingo == 1)
            {
                for (int i = 0; i < domingo; i++)
                {
                    decimal faltass = faltas;
                    decimal faltas2 = faltas3 + faltass;
                    resultado = faltas2;
                }
                return resultado;
            }


            else
            {
                decimal faltass = faltas3 * domingo;
                resultado = faltas + faltass;

                return resultado;

            }
            return faltas;
            
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.RH
{
    class FolhaPagamentoBusiness
    {
        FolhaPagamentoDatabase db = new FolhaPagamentoDatabase();

        public int Salvar(FolhaPagamentoDTO funcionario)
        {
            if (funcionario.Mes == "Selecione o mês")
            {
                throw new ArgumentException("Selecione um mês valido.");
            }
            if (funcionario.DSR > funcionario.Faltas)
            {
                throw new ArgumentException("O número de domingos não pode ser mair que o número de faltas.");
            }
            /*if ( funcionario.DSR > 4)
            {
                throw new ArgumentException("O número de domingos superior a 4.");
            }*/
            if (funcionario.Faltas >= 1 && funcionario.DSR == 0)
            {
                throw new ArgumentException("O número de domingos não pode ser 0.");
            }
            return db.Salvar(funcionario);
        }

       /* public void Alterar(FolhaPagamentoDTO funcionario)
        {


            db.Alterar(funcionario);
        }*/

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<FolhaPagamentoDTO> Consultar()
        {

            return db.Consultar();
        }



        public List<FolhaPagamentoDTO> Listar()
        {
            FolhaPagamentoDatabase db = new FolhaPagamentoDatabase();
            return db.Listar();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Cliente
{
    class ClienteEventualDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }        
        public string Endereço { get; set; }
        public string Numero { get; set; }
        public string CEP { get; set; }        
        public string Complemento { get; set; }        
        public string Telefone { get; set; }
        

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Cliente
{
    public class ClienteDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Endereço { get; set; }
        public string Numero { get; set; }
        public string CEP { get; set; }
        public string Celular { get; set; }
        public DateTime Nascimento { get; set; }
        public string Email { get; set; }
        public string Complemento { get; set; }
        public string RG { get; set; }
        public string Telefone { get; set; }
        public string Observacao { get; set; }


    }
}

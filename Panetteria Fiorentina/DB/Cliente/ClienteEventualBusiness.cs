﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Cliente
{
    class ClienteEventualBusiness
    {
     
        ClienteEventualDatabase db = new ClienteEventualDatabase();

      

        public int Salvar(ClienteEventualDTO funcionario)
        {
            


            if (funcionario.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (funcionario.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório.");
            }
            if (funcionario.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório.");
            }

            if (funcionario.Numero == string.Empty)
            {
                throw new ArgumentException("Número é obrigatório.");
            }
            if (funcionario.Endereço == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório.");
            }
       


          
            return db.Salvar(funcionario);
        }

        public void Alterar(ClienteEventualDTO cliente)
        {
            if (cliente.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

         


            if (cliente.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório.");
            }
            if (cliente.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório.");
            }

            if (cliente.Numero == string.Empty)
            {
                throw new ArgumentException("Número é obrigatório.");
            }
            if (cliente.Endereço == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório.");
            }
          
            db.Alterar(cliente);


        }
        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<ClienteEventualDTO> Consultar(string nome)
        {

            return db.Consultar(nome);
        }
        public List<ClienteEventualDTO> Listar()
        {

            return db. Listar();
        }
    }
}
    


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Cliente
{
    class CorreioCliente
    {
        public string cep { get; set; }
        public string Logradouro { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Cliente
{
    class ClienteBusiness
    {
        ClienteDatabase db = new ClienteDatabase();

        public ClienteDTO ConsultarPorCpf(string cpf)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.ConsultarPorCpf(cpf);
        }
        public ClienteDTO ConsultarPorRg (string rg)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.ConsultarPorRg(rg);
        }

        public int Salvar(ClienteDTO funcionario)
        {
            if (funcionario.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (funcionario.CPF == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório.");
            }


            if (funcionario.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório.");
            }
            if (funcionario.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório.");
            }

            if (funcionario.Numero == string.Empty)
            {
                throw new ArgumentException("Número é obrigatório.");
            }
            if (funcionario.Endereço == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório.");
            }
            ClienteDTO cliente = this.ConsultarPorCpf(funcionario.CPF);
            if (cliente != null)
            {
                throw new ArgumentException("CPF já cadastrado no sistema.");
            }
            ClienteDTO cliente2 = this.ConsultarPorRg(funcionario.RG);
            if (cliente != null)
            {
                throw new ArgumentException("RG já cadastrado no sistema.");
            }


            string a = funcionario.Email.ToString();
            bool b = a.Contains("@");
            bool c = a.Contains(".com");
            if (b == false && c == false)

            {
                throw new ArgumentException("Informe um email valido.");
            }
            return db.Salvar(funcionario);
        }

        public void Alterar(ClienteDTO cliente)
        {
            if (cliente.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (cliente.CPF == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório.");
            }


            if (cliente.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório.");
            }
            if (cliente.CEP == string.Empty)
            {
                throw new ArgumentException("CEP é obrigatório.");
            }

            if (cliente.Numero == string.Empty)
            {
                throw new ArgumentException("Número é obrigatório.");
            }
            if (cliente.Endereço == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório.");
            }
            /*ClienteDTO cliente1 = this.ConsultarPorCpf(cliente.CPF);
            if (cliente != null)
            {
                throw new ArgumentException("CPF já cadastrado no sistema.");
            }
            ClienteDTO cliente2 = this.ConsultarPorRg(cliente.RG);
            if (cliente != null)
            {
                throw new ArgumentException("RG já cadastrado no sistema.");
            }*/

            string a = cliente.Email.ToString();
            bool b = a.Contains("@");
            bool c = a.Contains(".com");
            if (b == false && c == false)

            {
                throw new ArgumentException("Informe um email valido.");
            }
            db.Alterar(cliente);


        }
        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<ClienteDTO> Consultar(string nome)
        {

            return db.Consultar(nome);
        }
        public List<ClienteDTO> Listar()
        {

            return db. Listar();
        }
    }
}

﻿using MySql.Data.MySqlClient;
using Panetteria_Fiorentina.DB.Base;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Cliente
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO cliente)
        {
            string script =
            @"INSERT INTO TB_CLIENTE
            (
	            NM_NOME,                                    
                DS_CPF,
                DS_ENDERECO,
                DS_NUMERO,
                DS_CEP,
                DS_CELULAR,
                DS_EMAIL,
                DATA_NASCIMENTO,
                DS_COMPLEMENTO,
                DS_TELEFONE,
                DS_RG,
                DS_OBSERVACAO
            )

            VALUES
            (
	            @NM_NOME,
                @DS_CPF,
                @DS_ENDERECO,
                @DS_NUMERO,
                @DS_CEP,
                @DS_CELULAR,
                @DS_EMAIL,
                @DATA_NASCIMENTO,
                @DS_COMPLEMENTO,
                @DS_TELEFONE,
                @DS_RG,
                @DS_OBSERVACAO

            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_NOME", cliente.Nome));
            parms.Add(new MySqlParameter("DS_CPF", cliente.CPF));
            parms.Add(new MySqlParameter("DS_ENDERECO", cliente.Endereço));
            parms.Add(new MySqlParameter("DS_NUMERO", cliente.Numero));
            parms.Add(new MySqlParameter("DS_CEP", cliente.CEP));
            parms.Add(new MySqlParameter("DS_EMAIL", cliente.Email));
            parms.Add(new MySqlParameter("DATA_NASCIMENTO", cliente.Nascimento));
            parms.Add(new MySqlParameter("DS_COMPLEMENTO", cliente.Complemento));
            parms.Add(new MySqlParameter("DS_TELEFONE", cliente.Telefone));
            parms.Add(new MySqlParameter("DS_RG", cliente.RG));
            parms.Add(new MySqlParameter("DS_OBSERVACAO", cliente.Observacao));
            parms.Add(new MySqlParameter("DS_CELULAR", cliente.Celular));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ClienteDTO cliente)
        {
            string script =
            @"UPDATE TB_CLIENTE 
                 SET 

                     ID_CLIENTE      =@ID_CLIENTE,
                     NM_NOME         = @NM_NOME,
	                 DS_CPF          = @DS_CPF,
	                 DS_ENDERECO     = @DS_ENDERECO,
	                 DS_CEP          = @DS_CEP,
	                 DS_NUMERO       = @DS_NUMERO,
	                 DS_EMAIL        = @DS_EMAIL,
	                 DATA_NASCIMENTO = @DATA_NASCIMENTO,
                     DS_COMPLEMENTO  = @DS_COMPLEMENTO,
                     DS_RG           = @DS_RG,
                     DS_CELULAR      = @DS_CELULAR, 
                     DS_TELEFONE     = @DS_TELEFONE,
                     DS_OBSERVACAO   =@DS_OBSERVACAO 
               WHERE ID_CLIENTE = @ID_CLIENTE";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_CLIENTE", cliente.ID));
            parms.Add(new MySqlParameter("NM_NOME", cliente.Nome));
            parms.Add(new MySqlParameter("DS_CPF", cliente.CPF));
            parms.Add(new MySqlParameter("DS_ENDERECO", cliente.Endereço));
            parms.Add(new MySqlParameter("DS_NUMERO", cliente.Numero));
            parms.Add(new MySqlParameter("DS_CEP", cliente.CEP));
            parms.Add(new MySqlParameter("DS_EMAIL", cliente.Email));
            parms.Add(new MySqlParameter("DATA_NASCIMENTO", cliente.Nascimento));
            parms.Add(new MySqlParameter("DS_COMPLEMENTO", cliente.Complemento));
            parms.Add(new MySqlParameter("DS_TELEFONE", cliente.Telefone));
            parms.Add(new MySqlParameter("DS_RG", cliente.RG));
            parms.Add(new MySqlParameter("DS_CELULAR", cliente.Celular));
            parms.Add(new MySqlParameter("DS_OBSERVACAO", cliente.Observacao));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_CLIENTE WHERE ID_CLIENTE = @ID_CLIENTE";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ClienteDTO> Consultar(string Nome)
        {
            string script =
            @"SELECT * 
                FROM TB_CLIENTE
               WHERE NM_NOME LIKE @NM_NOME";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_NOME", "%" + Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> cliente = new List<ClienteDTO>();

            while (reader.Read())
            {
                ClienteDTO novoCliente = new ClienteDTO();
                novoCliente.ID = reader.GetInt32("ID_CLIENTE");
                novoCliente.Nome = reader.GetString("NM_NOME");
                novoCliente.CPF = reader.GetString("DS_CPF");
                novoCliente.CEP = reader.GetString("DS_CEP");
                novoCliente.Numero = reader.GetString("DS_NUMERO");
                novoCliente.Email = reader.GetString("DS_EMAIL");
                novoCliente.Nascimento = reader.GetDateTime("DATA_NASCIMENTO");
                novoCliente.Complemento = reader.GetString("DS_COMPLEMENTO");
                novoCliente.Telefone = reader.GetString("DS_TELEFONE");
                novoCliente.RG = reader.GetString("DS_RG");
                novoCliente.Endereço = reader.GetString("DS_ENDERECO");
                novoCliente.Observacao = reader.GetString("DS_OBSERVACAO");
                novoCliente.Celular = reader.GetString("DS_CELULAR");
                cliente.Add(novoCliente);
            }
            reader.Close();

            return cliente;  
        }
        public List<ClienteDTO> Listar()
        {
            string script =
            @"SELECT * 
                FROM TB_CLIENTE;";
              

            List<MySqlParameter> parms = new List<MySqlParameter>();
            

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> cliente = new List<ClienteDTO>();

            while (reader.Read())
            {
                ClienteDTO novoCliente = new ClienteDTO();
                novoCliente.ID = reader.GetInt32("ID_CLIENTE");
                novoCliente.Nome = reader.GetString("NM_NOME");
                novoCliente.CPF = reader.GetString("DS_CPF");
                novoCliente.CEP = reader.GetString("DS_CEP");
                novoCliente.Numero = reader.GetString("DS_NUMERO");
                novoCliente.Email = reader.GetString("DS_EMAIL");
                novoCliente.Nascimento = reader.GetDateTime("DATA_NASCIMENTO");
                novoCliente.Complemento = reader.GetString("DS_COMPLEMENTO");
                novoCliente.Telefone = reader.GetString("DS_TELEFONE");
                novoCliente.RG = reader.GetString("DS_RG");
                novoCliente.Endereço = reader.GetString("DS_ENDERECO");
                novoCliente.Observacao = reader.GetString("DS_OBSERVACAO");
                novoCliente.Celular = reader.GetString("DS_CELULAR");

                cliente.Add(novoCliente);
            }
            reader.Close();

            return cliente;
        }

        public ClienteDTO ConsultarPorCpf(string cpf)
        {
            string script = @"SELECT * FROM TB_CLIENTE WHERE DS_CPF = @DS_CPF";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_CPF", cpf));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            ClienteDTO dto = null;
            if (reader.Read())
            {
                dto = new ClienteDTO();

                dto.ID = reader.GetInt32("ID_CLIENTE");
                dto.Nome = reader.GetString("NM_NOME");
                dto.CPF= reader.GetString("DS_CPF");
            }
            reader.Close();

            return dto;
        }
        public ClienteDTO ConsultarPorRg(string rg)
        {
            string script = @"SELECT * FROM TB_CLIENTE WHERE DS_RG = @DS_RG";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_RG", rg));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            ClienteDTO dto = null;
            if (reader.Read())
            {
                dto = new ClienteDTO();

                dto.ID = reader.GetInt32("ID_CLIENTE");
                dto.Nome = reader.GetString("NM_NOME");
                dto.CPF = reader.GetString("DS_RG");
            }
            reader.Close();

            return dto;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Fluxo_de_Caixa
{
    class FluxoCaixaBusiness
    {
        public List<FluxoCaixaDTO> Consultar(DateTime inicio, DateTime fim)
        {
            FluxoCaixaDatabase db = new FluxoCaixaDatabase();
            return db.Consultar(inicio, fim);

        }

    }
}

﻿using MySql.Data.MySqlClient;
using Panetteria_Fiorentina.DB.Base;
using Panetteria_Fiorentina.DB.Fluxo_de_Caixa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Fluxo_de_Caixa
{
    class FluxoCaixaDatabase
    {
        public List<FluxoCaixaDTO> Consultar(DateTime inicio, DateTime fim)
        {
            string script = @"SELECT * FROM VW_CONSULTAR_FLUXODECAIXA 
                 Where DT_REFERENCIA >= @inicio and dt_referencia <= @fim";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("inicio", inicio));
            parms.Add(new MySqlParameter("fim", fim));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FluxoCaixaDTO> lista = new List<FluxoCaixaDTO>();
            while (reader.Read())
            {
                FluxoCaixaDTO dto = new FluxoCaixaDTO();
                dto.DataReferencia = reader.GetDateTime("dt_referencia");
                dto.ValorGanhos = reader.GetDecimal("vl_total_ganhos");
                dto.ValorDespesas = reader.GetDecimal("vl_total_despesas");
                dto.ValorLucros = reader.GetDecimal("vl_saldo");

                lista.Add(dto);

            }
            reader.Close();

            return lista;

        }

       }
    }

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Fluxo_de_Caixa
{
    class GastosBusiness
    {
        GastosDatabase db = new GastosDatabase();

        public int Salvar(GastosDTO nome)
        {
            if (nome.Nome == "")
            {
                throw new ArgumentException("Nome é obrigatorio.");
            }
            if (nome.Valor == 0)
            {
                throw new ArgumentException("Defina o valor do gasto.");
            }

            if (nome.Tipo == "Selecione")
            {
                throw new ArgumentException("Selecione o tipo de gasto.");
            }
            return db.Salvar(nome);
        }

        public void Alterar(GastosDTO nome)
        {
            if (nome.Nome == "")
            {
                throw new ArgumentException("Nome é obrigatorio.");
            }
            if (nome.Valor == 0)
            {
                throw new ArgumentException("Defina o valor do gasto.");
            }

            if (nome.Tipo == "Selecione")
            {
                throw new ArgumentException("Selecione o tipo de gasto.");
            }



            db.Alterar(nome);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<GastosDTO> Consultar(DateTime inicio, DateTime fim)
        {

            return db.Consultar(inicio, fim);
        }



        public List<GastosDTO> Listar()
        {
           
            return db.Listar();
        }

    }
}

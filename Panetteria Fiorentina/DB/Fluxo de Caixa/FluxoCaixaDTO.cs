﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Fluxo_de_Caixa
{
    class FluxoCaixaDTO
    {
        public DateTime DataReferencia { get; set; }
        public decimal ValorGanhos { get; set; }
        public decimal ValorDespesas { get; set; }
        public decimal ValorLucros { get; set; }
    }

}

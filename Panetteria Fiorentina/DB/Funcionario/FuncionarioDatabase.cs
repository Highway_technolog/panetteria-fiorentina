﻿using MySql.Data.MySqlClient;
using Panetteria_Fiorentina.DB.Base;
using Panetteria_Fiorentina.DB.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Funcionario
{
    class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO Funcionario)
        {
            string Script = @"INSERT INTO TB_FUNCIONARIO
                            (
                                    NM_NOME,
                                    DS_CPF,
                                    DS_ENDERECO,
                                    DS_NUMERO,
                                    DS_CEP,
                                    DS_CELULAR,
                                    DT_NASCIMENTO,
                                    VL_SALARIO,
                                    DS_EMAIL,                     
                                    DS_COMPLEMENTO,
                                    DS_FOTO,
                                    DS_TELEFONE,
                                    DS_RG,
                                    DS_USUARIO,
                                    DS_SENHA,
                                    BT_ADMIN,
                                    BT_RH,
                                    BT_LOGISTICA,
                                    BT_FINANCEIRO,
                                    BT_VENDAS,
                                    BT_COMPRAS,
                                    BT_EMPREGADO,
                                    DS_CARGO
                                    
                            )
                            VALUES
                            (       
                                    @NM_NOME,
                                    @DS_CPF,
                                    @DS_ENDERECO,
                                    @DS_NUMERO,
                                    @DS_CEP,
                                    @DS_CELULAR,
                                    @DT_NASCIMENTO,
                                    @VL_SALARIO,
                                    @DS_EMAIL,                     
                                    @DS_COMPLEMENTO,
                                    @DS_FOTO,
                                    @DS_TELEFONE,
                                    @DS_RG,
                                    @DS_USUARIO,
                                    @DS_SENHA,
                                    @BT_ADMIN,
                                    @BT_RH,
                                    @BT_LOGISTICA,
                                    @BT_FINANCEIRO,
                                    @BT_VENDAS,
                                    @BT_COMPRAS,
                                    @BT_EMPREGADO,
                                    @DS_CARGO
                             );";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_NOME", Funcionario.Nome));
            parms.Add(new MySqlParameter("DS_CPF", Funcionario.CPF));
            parms.Add(new MySqlParameter("DS_ENDERECO", Funcionario.Endereço));
            parms.Add(new MySqlParameter("DS_NUMERO", Funcionario.Numero));
            parms.Add(new MySqlParameter("DS_CEP", Funcionario.CEP));
            parms.Add(new MySqlParameter("DS_CELULAR", Funcionario.Celular));
            parms.Add(new MySqlParameter("DT_NASCIMENTO", Funcionario.Nascimento));
            parms.Add(new MySqlParameter("VL_SALARIO", Funcionario.Salario));       
            parms.Add(new MySqlParameter("DS_EMAIL", Funcionario.Email));            
            parms.Add(new MySqlParameter("DS_COMPLEMENTO", Funcionario.Complemento));
            parms.Add(new MySqlParameter("DS_FOTO", Funcionario.Foto));
            parms.Add(new MySqlParameter("DS_TELEFONE", Funcionario.Telefone));
            parms.Add(new MySqlParameter("DS_RG", Funcionario.RG));
            parms.Add(new MySqlParameter("DS_USUARIO", Funcionario.Usuario));
            parms.Add(new MySqlParameter("DS_SENHA", Funcionario.Senha));
            parms.Add(new MySqlParameter("BT_ADMIN", Funcionario.Admin));
            parms.Add(new MySqlParameter("BT_RH", Funcionario.RH));
            parms.Add(new MySqlParameter("BT_LOGISTICA", Funcionario.Logistica));
            parms.Add(new MySqlParameter("BT_FINANCEIRO", Funcionario.Financeiro));
            parms.Add(new MySqlParameter("BT_VENDAS", Funcionario.Vendas));
            parms.Add(new MySqlParameter("BT_COMPRAS", Funcionario.Compras));
            parms.Add(new MySqlParameter("BT_EMPREGADO", Funcionario.Empregado));
            parms.Add(new MySqlParameter("DS_CARGO", Funcionario.Cargo));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(Script, parms);
            return pk;


        }
        public void Alterar(FuncionarioDTO Funcionario)
        {
            string script =
            @"UPDATE TB_FUNCIONARIO
                 SET                NM_NOME           = @NM_NOME,
                                    DS_CPF            = @DS_CPF,
                                    DS_ENDERECO       = @DS_ENDERECO,
                                    DS_NUMERO         = @DS_NUMERO,
                                    DS_CEP            = @DS_CEP,
                                    DT_NASCIMENTO     = @DT_NASCIMENTO,
                                    VL_SALARIO        = @VL_SALARIO, 
                                    DS_EMAIL          = @DS_EMAIL,        
                                    DS_COMPLEMENTO    = @DS_COMPLEMENTO,  
                                    DS_FOTO           = @DS_FOTO,   
                                    DS_TELEFONE       = @DS_TELEFONE, 
                                    DS_RG             = @DS_RG,    
                                    DS_USUARIO        = @DS_USUARIO, 
                                    DS_SENHA          = @DS_SENHA,
                                    BT_ADMIN          = @BT_ADMIN, 
                                    BT_RH             = @BT_RH,
                                    BT_LOGISTICA      = @BT_LOGISTICA, 
                                    BT_FINANCEIRO     = @BT_FINANCEIRO, 
                                    BT_VENDAS         = @BT_VENDAS,
                                    BT_COMPRAS        = @BT_COMPRAS,
                                    BT_EMPREGADO      = @BT_EMPREGADO,
                                    DS_CARGO          = @DS_CARGO
               WHERE ID_FUNCIONARIO = @ID_FUNCIONARIO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_NOME", Funcionario.Nome));
            parms.Add(new MySqlParameter("DS_CPF", Funcionario.CPF));
            parms.Add(new MySqlParameter("DS_ENDERECO", Funcionario.Endereço));
            parms.Add(new MySqlParameter("DS_NUMERO", Funcionario.Numero));
            parms.Add(new MySqlParameter("DS_CEP", Funcionario.CEP));
            parms.Add(new MySqlParameter("DS_CELULAR", Funcionario.Celular));
            parms.Add(new MySqlParameter("DT_NASCIMENTO", Funcionario.Nascimento));
            parms.Add(new MySqlParameter("VL_SALARIO", Funcionario.Salario));
            parms.Add(new MySqlParameter("DS_EMAIL", Funcionario.Email));
            parms.Add(new MySqlParameter("DS_COMPLEMENTO", Funcionario.Complemento));
            parms.Add(new MySqlParameter("DS_FOTO", Funcionario.Foto));
            parms.Add(new MySqlParameter("DS_TELEFONE", Funcionario.Telefone));
            parms.Add(new MySqlParameter("DS_RG", Funcionario.RG));
            parms.Add(new MySqlParameter("DS_USUARIO", Funcionario.Usuario));
            parms.Add(new MySqlParameter("BT_ADMIN", Funcionario.Admin));
            parms.Add(new MySqlParameter("BT_RH", Funcionario.RH));
            parms.Add(new MySqlParameter("BT_LOGISTICA", Funcionario.Logistica));
            parms.Add(new MySqlParameter("BT_FINANCEIRO", Funcionario.Financeiro));
            parms.Add(new MySqlParameter("BT_VENDAS", Funcionario.Vendas));
            parms.Add(new MySqlParameter("BT_COMPRAS", Funcionario.Compras));
            parms.Add(new MySqlParameter("BT_EMPREGADO", Funcionario.Empregado));
            parms.Add(new MySqlParameter("DS_CARGO", Funcionario.Cargo));
            parms.Add(new MySqlParameter("DS_SENHA", Funcionario.Senha));
            parms.Add(new MySqlParameter("ID_FUNCIONARIO", Funcionario.ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_FUNCIONARIO WHERE ID_FUNCIONARIO = @ID_FUNCIONARIO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_FUNCIONARIO", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<FuncionarioDTO> Consultar(string nome)
        {
            string script =
            @"SELECT * 
                FROM TB_FUNCIONARIO
                WHERE NM_NOME LIKE @NM_NOME
                 ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", "%" + nome + "%"));


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> funcionarios = new List<FuncionarioDTO>();

            while (reader.Read())
            {
                FuncionarioDTO novofuncionario = new FuncionarioDTO();
                novofuncionario.ID = reader.GetInt32("ID_FUNCIONARIO");
                novofuncionario.Nome = reader.GetString("NM_NOME");
                novofuncionario.CPF = reader.GetString("DS_CPF");
                novofuncionario.Endereço = reader.GetString("DS_ENDERECO");
                novofuncionario.Numero = reader.GetString("DS_NUMERO");
                novofuncionario.CEP = reader.GetString("DS_CEP");
                novofuncionario.Celular = reader.GetString("DS_CELULAR");
                novofuncionario.Nascimento = reader.GetDateTime("DT_NASCIMENTO");
                novofuncionario.Salario = reader.GetDecimal("VL_SALARIO");
                novofuncionario.Email = reader.GetString("DS_EMAIL");
                novofuncionario.Complemento = reader.GetString("DS_COMPLEMENTO");
              //  novofuncionario.Foto = reader.GetString("DS_FOTO");
                novofuncionario.RG = reader.GetString("DS_RG");
                novofuncionario.Telefone = reader.GetString("DS_TELEFONE");
                novofuncionario.Usuario = reader.GetString("DS_USUARIO");
                novofuncionario.Senha = reader.GetString("DS_SENHA");
                novofuncionario.Admin = reader.GetBoolean("BT_ADMIN");
                novofuncionario.RH = reader.GetBoolean("BT_RH");
                novofuncionario.Logistica = reader.GetBoolean("BT_LOGISTICA");
                novofuncionario.Financeiro = reader.GetBoolean("BT_FINANCEIRO");
                novofuncionario.Vendas = reader.GetBoolean("BT_VENDAS");
                novofuncionario.Compras = reader.GetBoolean("BT_COMPRAS");
                novofuncionario.Empregado = reader.GetBoolean("BT_EMPREGADO");
                novofuncionario.Cargo = reader.GetString("DS_CARGO");

                funcionarios.Add(novofuncionario);
            }
            reader.Close();

            return funcionarios;

        }

        public FuncionarioDTO Logar(string login, string senha)
        {
            string script = @"SELECT * FROM TB_FUNCIONARIO
                               WHERE DS_USUARIO = @DS_USUARIO
                                 AND DS_SENHA = @DS_SENHA";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_USUARIO", login));
            parms.Add(new MySqlParameter("DS_SENHA", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO funcionario = null;

            if (reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.ID = reader.GetInt32("ID_FUNCIONARIO");
                funcionario.Nome = reader.GetString("NM_NOME");
                funcionario.CPF = reader.GetString("DS_CPF");
                funcionario.Endereço = reader.GetString("DS_ENDERECO");
                funcionario.Numero = reader.GetString("DS_NUMERO");
                funcionario.CEP = reader.GetString("DS_CEP");
                funcionario.Celular = reader.GetString("DS_CELULAR");
                funcionario.Nascimento = reader.GetDateTime("DT_NASCIMENTO");
                funcionario.Salario = reader.GetDecimal("VL_SALARIO");
                funcionario.Email = reader.GetString("DS_EMAIL");
                funcionario.Complemento = reader.GetString("DS_COMPLEMENTO");
                //funcionario.Foto = reader.GetString("DS_FOTO");
                funcionario.RG = reader.GetString("DS_RG");
                funcionario.Telefone = reader.GetString("DS_TELEFONE");
                funcionario.Usuario = reader.GetString("DS_USUARIO");
                funcionario.Senha = reader.GetString("DS_SENHA");
                funcionario.Admin = reader.GetBoolean("BT_ADMIN");
                funcionario.RH = reader.GetBoolean("BT_RH");
                funcionario.Logistica = reader.GetBoolean("BT_LOGISTICA");
                funcionario.Financeiro = reader.GetBoolean("BT_FINANCEIRO");
                funcionario.Vendas = reader.GetBoolean("BT_VENDAS");
                funcionario.Compras = reader.GetBoolean("BT_COMPRAS");
                funcionario.Empregado = reader.GetBoolean("BT_EMPREGADO");

               

            }

            reader.Close();

            return funcionario;
        }

        public List<FuncionarioDTO> Listar()
        {
            string script = @"SELECT * FROM TB_FUNCIONARIO
                                WHERE ID_FUNCIONARIO > 1";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {

                FuncionarioDTO funcionario = new FuncionarioDTO();
                funcionario.ID = reader.GetInt32("ID_FUNCIONARIO");
                funcionario.Nome = reader.GetString("NM_NOME");
                funcionario.CPF = reader.GetString("DS_CPF");
                funcionario.Endereço = reader.GetString("DS_ENDERECO");
                funcionario.Numero = reader.GetString("DS_NUMERO");
                funcionario.CEP = reader.GetString("DS_CEP");
                funcionario.Celular = reader.GetString("DS_CELULAR");
                funcionario.Nascimento = reader.GetDateTime("DT_NASCIMENTO");
                funcionario.Salario = reader.GetDecimal("VL_SALARIO");
                funcionario.Email = reader.GetString("DS_EMAIL");
                funcionario.Complemento = reader.GetString("DS_COMPLEMENTO");
                
                funcionario.RG = reader.GetString("DS_RG");
                funcionario.Telefone = reader.GetString("DS_TELEFONE");
                funcionario.Usuario = reader.GetString("DS_USUARIO");
                funcionario.Admin = reader.GetBoolean("BT_ADMIN");
                funcionario.RH = reader.GetBoolean("BT_RH");
                funcionario.Logistica = reader.GetBoolean("BT_LOGISTICA");
                funcionario.Financeiro = reader.GetBoolean("BT_FINANCEIRO");
                funcionario.Vendas = reader.GetBoolean("BT_VENDAS");
                funcionario.Compras = reader.GetBoolean("BT_COMPRAS");
                funcionario.Empregado = reader.GetBoolean("BT_EMPREGADO");


                lista.Add(funcionario);
            }
            reader.Close();

            return lista;



        }
        public FuncionarioDTO ConsultarPorCpf(string cpf)
        {
            string script = @"SELECT * FROM TB_FUNCIONARIO WHERE DS_CPF = @DS_CPF";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_CPF", cpf));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO dto = null;
            if (reader.Read())
            {
                dto = new FuncionarioDTO();

                dto.ID = reader.GetInt32("ID_FUNCIONARIO");
                dto.Nome = reader.GetString("NM_NOME");
                dto.CPF = reader.GetString("DS_CPF");
            }
            reader.Close();

            return dto;
        }
        public FuncionarioDTO ConsultarPorRg(string rg)
        {
            string script = @"SELECT * FROM TB_FUNCIONARIO WHERE DS_RG = @DS_RG";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_RG", rg));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO dto = null;
            if (reader.Read())
            {
                dto = new FuncionarioDTO();

                dto.ID = reader.GetInt32("ID_FUNCIONARIO");
                dto.Nome = reader.GetString("NM_NOME");
                dto.CPF = reader.GetString("DS_RG");
            }
            reader.Close();

            return dto;
        }
        public FuncionarioDTO ConsultarPorUsuario(string Usuario)
        {
            string script = @"SELECT * FROM TB_FUNCIONARIO WHERE DS_USUARIO = @DS_USUARIO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_USUARIO", Usuario));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO dto = null;
            if (reader.Read())
            {
                dto = new FuncionarioDTO();

                dto.ID = reader.GetInt32("ID_FUNCIONARIO");
                dto.Nome = reader.GetString("NM_NOME");
                dto.CPF = reader.GetString("DS_USUARIO");
            }
            reader.Close();

            return dto;
        }

    }
}
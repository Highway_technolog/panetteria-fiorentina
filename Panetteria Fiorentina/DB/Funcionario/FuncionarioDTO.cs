﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Funcionario
{
    public class FuncionarioDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Endereço { get; set; }
        public string CEP { get; set; }
        public string Celular { get; set; }
        public DateTime Nascimento { get; set; }       
        public decimal Salario { get; set; }
        public string Email { get; set; }
        public string Complemento { get; set; }
        public string Foto { get; set; }
        public string RG { get; set; }        
        public string Numero { get; set; }        
        public string Telefone { get; set; }
        public string Cargo { get; set; }
        public string Usuario { get; set; }
        public string Senha { get; set; }
        public bool Admin { get; set; }
        public bool RH { get; set; }
        public bool Logistica { get; set; }
        public bool Financeiro { get; set; }
        public bool Vendas { get; set; }
        public bool Compras { get; set; }
        public bool Empregado { get; set; }

    }
}

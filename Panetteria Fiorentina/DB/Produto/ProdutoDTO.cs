﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Produto
{
    public class ProdutoDTO
    {
        public int Id { get; set; }
        
        public decimal ValorUnitario { get; set; }
        public string Tipo { get; set; }
        public string FornecedorNome { get; set; }
        public int IdFornecedor { get; set; }
        public string Nome { get; set; }
       
    }
}

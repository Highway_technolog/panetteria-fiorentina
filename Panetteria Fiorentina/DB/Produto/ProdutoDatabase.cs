﻿using MySql.Data.MySqlClient;
using Panetteria_Fiorentina.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO produto)
        {
            string script =
            @"INSERT INTO TB_PRODUTO
            (
	            NM_PRODUTO,                                    
                DS_PRECOUNITARIO,
                DS_TIPO,
                ID_FORNECEDOR,
               
                NM_FORNECEDOR
                
            )
            VALUES
            (
	            @NM_PRODUTO,                                    
                @DS_PRECOUNITARIO,
                @DS_TIPO,
                @ID_FORNECEDOR,
              
                @NM_FORNECEDOR
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_PRECOUNITARIO", produto.ValorUnitario));
            
            
            parms.Add(new MySqlParameter("DS_TIPO", produto.Tipo));
            parms.Add(new MySqlParameter("ID_FORNECEDOR", produto.IdFornecedor));
          
            parms.Add(new MySqlParameter("NM_PRODUTO", produto.Nome));
            parms.Add(new MySqlParameter("NM_FORNECEDOR", produto.FornecedorNome));
            

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ProdutoDTO produto)
        {
            string script =
            @"UPDATE TB_PRODUTO
                 SET    
                        NM_PRODUTO        =   @NM_PRODUTO,                              
                        DS_PRECOUNITARIO  =   @DS_PRECOUNITARIO,
                        DS_TIPO           =   @DS_TIPO,
                        ID_FORNECEDOR     =   @ID_FORNECEDOR,
                 
                        NM_FORNECEDOR     =   @NM_FORNECEDOR
              ";

            
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_PRODUTO", produto.Nome));
            parms.Add(new MySqlParameter("DS_PRECOUNITARIO", produto.ValorUnitario));
            parms.Add(new MySqlParameter("DS_TIPO", produto.Tipo));
            parms.Add(new MySqlParameter("ID_FORNECEDOR", produto.IdFornecedor));

            parms.Add(new MySqlParameter("NM_FORNECEDOR", produto.FornecedorNome));
            



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_PRODUTO WHERE ID_PRODUTO = @ID_PRODUTO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PRODUTO", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void RemoverFornecedor(int id)
        {
            string script =
            @"DELETE FROM TB_PRODUTO WHERE ID_FORNECEDOR = @ID_FORNECEDOR";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_FORNECEDOR", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoDTO> Consultar()
        {
            string script =
            @"SELECT * 
                FROM TB_PRODUTO
                WHERE ID_PRODUTO > 1
               ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> produto = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO novoProduto = new ProdutoDTO();
                novoProduto.Id = reader.GetInt32("ID_PRODUTO");
                novoProduto.Nome = reader.GetString("NM_PRODUTO");                
                novoProduto.ValorUnitario = reader.GetDecimal("DS_PRECOUNITARIO");                
                novoProduto.Tipo = reader.GetString("DS_TIPO");               
                novoProduto.IdFornecedor = reader.GetInt32("ID_FORNECEDOR");
                novoProduto.FornecedorNome = reader.GetString("NM_FORNECEDOR");
               

                produto.Add(novoProduto);
            }
            reader.Close();

            return produto;
        }
        
        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM TB_PRODUTO
                              WHERE ID_PRODUTO > 1
                               ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {

                ProdutoDTO novoProduto = new ProdutoDTO();
                novoProduto.Id = reader.GetInt32("ID_PRODUTO");
                novoProduto.Nome = reader.GetString("NM_PRODUTO");
                novoProduto.ValorUnitario = reader.GetDecimal("DS_PRECOUNITARIO");
                novoProduto.Tipo = reader.GetString("DS_TIPO");
                novoProduto.IdFornecedor = reader.GetInt32("ID_FORNECEDOR");
                novoProduto.FornecedorNome = reader.GetString("NM_FORNECEDOR");



                lista.Add(novoProduto);
            }
            reader.Close();

            return lista;



        }
        public List<ProdutoDTO> ListarPorFornecedor(int id)
        {
            string script = @"SELECT * FROM TB_PRODUTO
                               WHERE ID_FORNECEDOR = @ID_FORNECEDOR
                               ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_FORNECEDOR", id));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {

                ProdutoDTO novoProduto = new ProdutoDTO();
                novoProduto.Id = reader.GetInt32("ID_PRODUTO");
                novoProduto.Nome = reader.GetString("NM_PRODUTO");
                novoProduto.ValorUnitario = reader.GetDecimal("DS_PRECOUNITARIO");
                novoProduto.Tipo = reader.GetString("DS_TIPO");
                novoProduto.IdFornecedor = reader.GetInt32("ID_FORNECEDOR");


                lista.Add(novoProduto);
            }
            reader.Close();

            return lista;



        }
    }
}

﻿using Panetteria_Fiorentina.DB.Cliente;
using Panetteria_Fiorentina.DB.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Produto
{
    class ProdutoBusiness
    {
        ProdutoDatabase db = new ProdutoDatabase();

        public int Salvar(ProdutoDTO nome)
        {
            if (nome.Nome == "")
            {
                throw new ArgumentException("Nome é obrigatorio.");
            }
            if (nome.Tipo == "Selecione")
            {
                throw new ArgumentException("Selecione o tipo.");
            }
            if (nome.ValorUnitario ==  0 )
            {
                throw new ArgumentException("Defina o preço para o produto.");
            }

            EstoqueDTO dto = new EstoqueDTO();
            dto.Ultima = DateTime.Now.Date;
            int id =  db.Salvar(nome);
            dto.IdProduto = id;
            dto.Produto = nome.Nome;
            dto.Quantidade = 0;
            dto.IdFornecedor = nome.IdFornecedor;
            dto.Fornecedor = nome.FornecedorNome;

            EstoqueBusiness bus = new EstoqueBusiness();
            return bus.Salvar(dto);

        }

         public void Alterar(ProdutoDTO nome)
         {
            if (nome.Nome == "")
            {
                throw new ArgumentException("Nome é obrigatorio.");
            }
           if (nome.ValorUnitario == 0)
            {
                throw new ArgumentException("Defina um valor para o produto");
            }
            if (nome.Tipo == "Selecione")
            {
                throw new ArgumentException("Selecione o tipo.");
            }



            db.Alterar(nome);
         }

        public void Remover(int id)
        {
           
            db.Remover(id);
        }
        public void RemoverFornecedor(int id)
        {
            db.RemoverFornecedor(id);
        }


        public List<ProdutoDTO> Consultar()
        {

            return db.Consultar();
        }



        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }
        public List<ProdutoDTO> ListarPorFornecedor(int id)
        {
            return db.ListarPorFornecedor(id);
        }

    }
}

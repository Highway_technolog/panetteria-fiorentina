﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Compra
{
    class VwPedidoDTO
    {
        public DateTime Data { get; set; }
        public decimal Valor { get; set; }
        public string Fornecedor { get; set; }
        public string Produto { get; set; }
    }
}

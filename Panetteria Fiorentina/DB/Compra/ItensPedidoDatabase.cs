﻿using MySql.Data.MySqlClient;
using Panetteria_Fiorentina.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Compra
{
    class ItensPedidoDatabase
    {
        public int Salvar(ItensPedidoDTO pedido)
        {
            string script =
            @"INSERT INTO TB_ITENSPEDIDO
            (
	            
                ID_PEDIDO,
                ID_PRODUTO
                
            )
            VALUES
            (
	            @ID_PEDIDO,
                @ID_PRODUTO
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PEDIDO", pedido.IdPedido));
            parms.Add(new MySqlParameter("ID_PRODUTO", pedido.IdProduto));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        /*public void Alterar(ProdutoDTO produto)
        {
            string script =
            @"UPDATE TB_PRODUTO
                 SET    NM_PRODUTO        =   @NM_PRODUTO,                              
                        DS_PRECOUNITARIO  =   @DS_PRECOUNITARIO,
                        DS_TIPO           =   @DS_TIPO,
                        ID_FORNECEDOR     =   @ID_FORNECEDOR,
                        DS_FOTO           =   @DS_FOTO,
                        NM_FORNECEDOR     =   @NM_FORNECEDOR
               WHERE ID_PRODUTO = @ID_PRODUTO";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_PRODUTO", produto.Nome));
            parms.Add(new MySqlParameter("DS_PRECOUNITARIO", produto.ValorUnitario));
            parms.Add(new MySqlParameter("DS_TIPO", produto.Tipo));
            parms.Add(new MySqlParameter("ID_FORNECEDOR", produto.IdFornecedor));
            parms.Add(new MySqlParameter("DS_FOTO", produto.Foto));
            parms.Add(new MySqlParameter("NM_FORNECEDOR", produto.FornecedorNome));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }*/

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_ITENSPEDIDO WHERE ID_PEDIDO = @ID_PEDIDO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PEDIDO", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void RemoverProduto(int id)
        {
            string script =
            @"DELETE FROM TB_ITENSPEDIDO WHERE ID_PRODUTO = @ID_PRODUTO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PRODUTO", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ItensPedidoDTO> Consultar()
        {
            string script =
            @"SELECT * 
                FROM TB_ITENSPEDIDO
              ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ItensPedidoDTO> pedido = new List<ItensPedidoDTO>();

            while (reader.Read())
            {
                ItensPedidoDTO novoProduto = new ItensPedidoDTO();
              /*  novoProduto.Id = reader.GetInt32("ID_PEDIDO");
                novoProduto.Data = reader.GetDateTime("DT_DATA");
                novoProduto.IdFornecedor = reader.GetInt32("ID_FORNECEDOR");
                novoProduto.Valor = reader.GetDecimal("DS_VALOR");
                novoProduto.NomeFornecedor = reader.GetString("NM_FORNECEDOR");*/


                pedido.Add(novoProduto);
            }
            reader.Close();

            return pedido;
            
        }
        
    }
}

﻿using MySql.Data.MySqlClient;
using Panetteria_Fiorentina.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Cliente
{
    class ItensEncomendaDatabase
    {
        public int Salvar(ItensEncomendaDTO ItensEncomenda)
        {
            string script =
            @"INSERT INTO TB_ITENS_ENCOMENDA
            (
	                                                
                ID_PRODUCAO,
                ID_ENCOMENDA
            )
            VALUES
            (
	                                               
                @ID_PRODUCAO,
                @ID_ENCOMENDA
            )";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PRODUCAO", ItensEncomenda.IdProducao));
            parms.Add(new MySqlParameter("ID_ENCOMENDA", ItensEncomenda.IdEncomenda));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        /*public void Alterar(ItensEncomendaDTO ItensEncomenda)
        {
            string script =
           @"UPDATE TB_ITENS_ENCOMENDA 
                 SET ID_PRODUCAO         = @ID_PRODUCAO,
	                ID_ENCOMENDA           = @ID_ENCOMENDAO,
	                

               WHERE ID_ITENS_ENCOMENDA = @ID_ITENS_ENCOMENDA";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_ITENS_ENCOMENDA", ItensEncomenda.Id));
            parms.Add(new MySqlParameter("ns_quantidade", ItensEncomenda.Quantidade));
            parms.Add(new MySqlParameter("id_encomenda", ItensEncomenda.IdEncomenda));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }*/

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_ITENS_ENCOMENDA WHERE ID_ITENS_ENCOMENDA = @ID_ITENS_ENCOMENDA";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_ITENS_ENCOMENDA", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ItensEncomendaDTO> Consultar(int id)
        {
            string script =
            @"SELECT * 
                FROM TB_ITENS_ENCOMENDA
               WHERE ID_ITENS_ENCOMENDA LIKE @ID_ITENS_ENCOMENDA";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_ITENS_ENCOMENDA", "%" + id + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ItensEncomendaDTO> encomenda = new List<ItensEncomendaDTO>();

            while (reader.Read())
            {
                ItensEncomendaDTO novoEncomenda = new ItensEncomendaDTO();
                novoEncomenda.IdItensEncomenda = reader.GetInt32("ID_ITENS_ENCOMENDA");
                novoEncomenda.IdProducao = reader.GetInt32("ID_PRODUCAO");
            
                novoEncomenda.IdEncomenda = reader.GetInt32("ID_ENCOMENDA");


                encomenda.Add(novoEncomenda);
            }
            reader.Close();

            return encomenda;
        }
    }
}


﻿using MySql.Data.MySqlClient;
using Panetteria_Fiorentina.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Cliente
{
    class EncomendaDatabase
    {
        public int Salvar(EncomendaDTO encomenda)
        {
            string script =
            @"INSERT INTO TB_ENCOMENDA
            (
	            DT_DATA,
                DS_VALOR,
                DS_STATUS,
                ID_CLIENTE,
                ID_CLIENTEEVENTUAL,
                NM_CLIENTE,
                DS_PAGAMENTO,
                DS_ENTREGA
            )
            VALUES
            (
                @DT_DATA,
                @DS_VALOR,
                @DS_STATUS,
                @ID_CLIENTE,
	            @ID_CLIENTEEVENTUAL,
                @NM_CLIENTE,
                @DS_PAGAMENTO,
                @DS_ENTREGA
            )";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DT_DATA", encomenda.Data));
            parms.Add(new MySqlParameter("DS_VALOR", encomenda.Valor));
            parms.Add(new MySqlParameter("NM_CLIENTE", encomenda.Cliente));
            parms.Add(new MySqlParameter("DS_PAGAMENTO", encomenda.Pagamento));
            parms.Add(new MySqlParameter("DS_STATUS", encomenda.Status));
            parms.Add(new MySqlParameter("DS_ENTREGA", encomenda.Entrega));
            if (encomenda.IdCliente == 0)
            {
                parms.Add(new MySqlParameter("ID_CLIENTE", DBNull.Value));
            }
            else
            {
                parms.Add(new MySqlParameter("ID_CLIENTE", encomenda.IdCliente));
            }
            

            if (encomenda.IdClienteEventual == 0)
            {
                parms.Add(new MySqlParameter("ID_CLIENTEEVENTUAL", DBNull.Value));
            }
            else
            {
                parms.Add(new MySqlParameter("ID_CLIENTEEVENTUAL", encomenda.IdClienteEventual));
            }
            



            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(int id)
        {
            string script =
            @"UPDATE TB_ENCOMENDA
                 SET DS_STATUS = 'Feito'
                WHERE ID_ENCOMENDA = @id
                     
                   ";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_ENCOMENDA WHERE ID_ENCOMENDA = @ID_ENCOMENDA";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_encomenda", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<EncomendaDTO> Consultar(DateTime inicio,DateTime fim)
        {
            string script =
            @"SELECT * 
                FROM TB_ENCOMENDA
                WHERE DT_DATA >= @inicio and DT_DATA <=@fim     
               ";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("inicio", inicio));
            parms.Add(new MySqlParameter("fim", fim));



            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EncomendaDTO> encomenda1 = new List<EncomendaDTO>();


            while (reader.Read())
            {
                EncomendaDTO novoEncomenda = new EncomendaDTO();
                novoEncomenda.ID = reader.GetInt32("ID_ENCOMENDA");
                novoEncomenda.Data = reader.GetDateTime("DT_DATA");
                novoEncomenda.Valor = reader.GetInt32("DS_VALOR");
                novoEncomenda.Cliente = reader.GetString("NM_CLIENTE");
                novoEncomenda.Pagamento= reader.GetString("DS_PAGAMENTO");
                novoEncomenda.Entrega= reader.GetString("DS_ENTREGA");
                novoEncomenda.Status = reader.GetString("DS_STATUS");
                //novoEncomenda.IdCliente = reader.GetInt32("ID_CLIENTE");
                //novoEncomenda.IdClienteEventual = reader.GetInt32("ID_CLIENTEEVENTUAL");....



                encomenda1.Add(novoEncomenda);
            }
            reader.Close();

            return encomenda1;
        }
        public List<EncomendaDTO> ConsultarStatus()
        {
            string script =
            @"SELECT * 
                FROM TB_ENCOMENDA
                  WHERE DS_STATUS = 'Pendente'
               ";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            



            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EncomendaDTO> encomenda1 = new List<EncomendaDTO>();


            while (reader.Read())
            {
                EncomendaDTO novoEncomenda = new EncomendaDTO();
                novoEncomenda.ID = reader.GetInt32("ID_ENCOMENDA");
                novoEncomenda.Data = reader.GetDateTime("DT_DATA");
                novoEncomenda.Valor = reader.GetInt32("DS_VALOR");
                novoEncomenda.Cliente = reader.GetString("NM_CLIENTE");
                novoEncomenda.Pagamento = reader.GetString("DS_PAGAMENTO");
                novoEncomenda.Entrega = reader.GetString("DS_ENTREGA");
                novoEncomenda.Status = reader.GetString("DS_STATUS");
                //novoEncomenda.IdCliente = reader.GetInt32("ID_CLIENTE");
                //novoEncomenda.IdClienteEventual = reader.GetInt32("ID_CLIENTEEVENTUAL");....



                encomenda1.Add(novoEncomenda);
            }
            reader.Close();

            return encomenda1;
        }





    }
}

   
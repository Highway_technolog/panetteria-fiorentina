﻿using MySql.Data.MySqlClient;
using Panetteria_Fiorentina.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Cliente
{
    class ProduçãoDatabase
    {
        public int Salvar(ProduçãoDTO cliente)
        {
            string script =
            @"INSERT INTO TB_PRODUCAO
            (
	            DS_DESCRICAO,                                    
                NM_PRODUCAO,
                VL_PRODUCAO,
                DS_TIPO
               
              
               
            )
            VALUES
            (
	            @DS_DESCRICAO,                                    
                @NM_PRODUCAO,
                @VL_PRODUCAO,
                @DS_TIPO
              
                
               
             
            )";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_DESCRICAO", cliente.Descricao));
            parms.Add(new MySqlParameter("NM_PRODUCAO", cliente.Nome));
            parms.Add(new MySqlParameter("VL_PRODUCAO", cliente.Valor));
            parms.Add(new MySqlParameter("DS_TIPO", cliente.Tipo));
            
          
            


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ProduçãoDTO cliente)
        {
            string script =
            @"UPDATE TB_PRODUCAO
                 SET  DS_DESCRICAO  =  @DS_DESCRICAO,                                 
                      NM_PRODUCAO   =  @NM_PRODUCAO,
                      VL_PRODUCAO  =  @VL_PRODUCAO,
                    
                     
                        DS_TIPO =     @DS_TIPO
               
                    ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_DESCRICAO", cliente.Descricao));
            parms.Add(new MySqlParameter("NM_PRODUCAO", cliente.Nome));
            parms.Add(new MySqlParameter("VL_PRODUCAO", cliente.Valor));
            parms.Add(new MySqlParameter("DS_TIPO", cliente.Tipo));
            
      




            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_PRODUCAO WHERE ID_PRODUCAO = @ID_PRODUCAO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PRODUCAO", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void RemoverPorProduto(int id)
        {
            string script =
            @"DELETE FROM TB_PRODUCAO WHERE ID_PRODUTO = @ID_PRODUTO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PRODUTO", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProduçãoDTO> Consultar()
        {
            string script =
            @"SELECT * 
                FROM TB_PRODUCAO
              ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
           

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProduçãoDTO> cliente = new List<ProduçãoDTO>();

            while (reader.Read())
            {
                ProduçãoDTO novoCliente = new ProduçãoDTO();
                novoCliente.Id = reader.GetInt32("ID_PRODUCAO");
                novoCliente.Nome = reader.GetString("NM_PRODUCAO");
                novoCliente.Valor = reader.GetDecimal("VL_PRODUCAO");
              
                novoCliente.Descricao = reader.GetString("DS_DESCRICAO");
              
                cliente.Add(novoCliente);
            }
            reader.Close();

            return cliente;
        }
        public List<ProduçãoDTO> Listar()
        {
            string script =
            @"SELECT * 
                FROM TB_PRODUCAO
               ;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
           

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProduçãoDTO> cliente = new List<ProduçãoDTO>();

            while (reader.Read())
            {
                ProduçãoDTO novoCliente = new ProduçãoDTO();
                novoCliente.Id = reader.GetInt32("ID_PRODUCAO");
                novoCliente.Nome = reader.GetString("NM_PRODUCAO");
                novoCliente.Valor = reader.GetDecimal("VL_PRODUCAO");
               
                novoCliente.Descricao = reader.GetString("DS_DESCRICAO");
            
                
               
               
                

                cliente.Add(novoCliente);
            }
            reader.Close();

            return cliente;
        }
    }
}

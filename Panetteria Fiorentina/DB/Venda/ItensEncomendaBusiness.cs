﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Cliente
{
    class ItensEncomendaBusiness
    {
        ItensEncomendaDatabase db = new ItensEncomendaDatabase();

        public int Salvar (ItensEncomendaDTO nome)
        {
            return db.Salvar(nome);
        }

        public void Remover(int id)
        {
             db.Remover(id);
        }
        public List<ItensEncomendaDTO> Consultar (int id)
        {
            return db.Consultar(id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Cliente
{
    class ProduçãoBusiness
    {
        ProduçãoDatabase db = new ProduçãoDatabase();

        public int Salvar (ProduçãoDTO pro)
        {

            if(pro.Nome==string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }
            if (pro.Tipo == "Selecione")
            {
                throw new ArgumentException("Selecione um tipo.");
            }
            if (pro.Valor == 0)
            {
                throw new ArgumentException("Valor é obrigatório.");
            }
            return db.Salvar(pro);

        }

        public void Alterar (ProduçãoDTO pro)
        {
            if (pro.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }
            if (pro.Valor == 0)
            {
                throw new ArgumentException("Valor é obrigatório.");
            }
            if (pro.Tipo == "Selecione")
            {
                throw new ArgumentException("Selecione um tipo.");
            }
            db.Alterar(pro);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }
        public void RemoverPorProduto(int id)
        {
            db.RemoverPorProduto(id);
        }

        public List<ProduçãoDTO> Consultar()
        {
            return db.Consultar();
        }
        public List<ProduçãoDTO> Listar()
        {
            return db.Listar();
        }
    }
}

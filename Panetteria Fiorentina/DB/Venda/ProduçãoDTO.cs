﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Cliente
{
    public class ProduçãoDTO
    {
       

        public int Id { get; set; }
        public string Descricao { get; set; }
        public string Nome { get; set; }
        public decimal Valor { get; set; }
       
        public string Tipo { get; set; }
        public int Idproduto { get; set; }
        public string Produto { get; set; }


    }
}

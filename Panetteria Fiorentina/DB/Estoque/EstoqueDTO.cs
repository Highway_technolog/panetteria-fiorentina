﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Estoque
{
    public class EstoqueDTO
    {
        public int Id { get; set; }
        public int Quantidade { get; set; }
        public DateTime Ultima { get; set; }
        public int IdProduto { get; set; }
        public string Produto { get; set; }
        public int IdFornecedor { get; set; }
        public string Fornecedor { get; set; }




    }
}

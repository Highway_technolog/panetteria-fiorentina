﻿using MySql.Data.MySqlClient;
using Panetteria_Fiorentina.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Estoque
{
    class EstoqueDatabase
    {
        public int Salvar(EstoqueDTO estoque)
        {
            string script =
            @"INSERT INTO TB_ESTOQUE
            (
	            DS_QUANTIDADE,
                DT_ULTIMA,
                ID_PRODUTO,
                NM_PRODUTO,
                ID_FORNECEDOR,
                NM_FORNECEDOR
            )
            VALUES
            (
	            @DS_QUANTIDADE,
                @DT_ULTIMA,
                @ID_PRODUTO,
                @NM_PRODUTO,
                @ID_FORNECEDOR,
                @NM_FORNECEDOR
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_QUANTIDADE", estoque.Quantidade));
            parms.Add(new MySqlParameter("DT_ULTIMA", estoque.Ultima));
            parms.Add(new MySqlParameter("ID_PRODUTO", estoque.IdProduto));
            parms.Add(new MySqlParameter("NM_PRODUTO", estoque.Produto));
            parms.Add(new MySqlParameter("ID_FORNECEDOR", estoque.IdFornecedor));
            parms.Add(new MySqlParameter("NM_FORNECEDOR", estoque.Fornecedor));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(EstoqueDTO cliente)
        {
            string script =
            @"UPDATE TB_PRODUCAO
                 SET  NM_PRODUTO  =  @NM_PRODUTO,                                 
                     NM_FORNECEDOR   =  @NM_FORNECEDOR,
                     
                    ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_PRODUTO", cliente.Produto));
            parms.Add(new MySqlParameter("NM_FORNECEDOR", cliente.Fornecedor));
          





            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public void Remover(int id)
        {
            string script =
            @"DELETE FROM TB_ESTOQUE WHERE ID_PRODUTO = @ID_PRODUTO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PRODUTO", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void RemoverFornecedor(int id)
        {
            string script =
            @"DELETE FROM TB_ESTOQUE WHERE ID_FORNECEDOR = @ID_FORNECEDOR";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_FORNECEDOR", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<EstoqueDTO> Consultar()
        {
            string script =
            @"SELECT * 
                FROM TB_ESTOQUE
               ";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> estoque = new List<EstoqueDTO>();

            while (reader.Read())
            {
                EstoqueDTO novoEstoque = new EstoqueDTO();
                novoEstoque.Id = reader.GetInt32("ID_ESTOQUE");
                novoEstoque.Quantidade = reader.GetInt32("DS_QUANTIDADE");
                novoEstoque.Ultima = reader.GetDateTime("DT_ULTIMA");
                novoEstoque.IdProduto = reader.GetInt32("ID_PRODUTO");
                novoEstoque.Produto = reader.GetString("NM_PRODUTO");
                novoEstoque.IdFornecedor = reader.GetInt32("ID_FORNECEDOR");
                novoEstoque.Fornecedor = reader.GetString("NM_FORNECEDOR");


                estoque.Add(novoEstoque);
            }
            reader.Close();

            return estoque;
        }
        public List<EstoqueDTO> ConsultarPorNome(string nome)
        {
            string script =
            @"SELECT * 
                FROM TB_ESTOQUE
                WHERE NM_PRODUTO = @NM_PRODUTO
               ";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("NM_PRODUTO",nome));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> estoque = new List<EstoqueDTO>();

            while (reader.Read())
            {
                EstoqueDTO novoEstoque = new EstoqueDTO();
                novoEstoque.Id = reader.GetInt32("ID_ESTOQUE");
                novoEstoque.Quantidade = reader.GetInt32("DS_QUANTIDADE");
                novoEstoque.Ultima = reader.GetDateTime("DT_ULTIMA");
                novoEstoque.IdProduto = reader.GetInt32("ID_PRODUTO");
                novoEstoque.Produto = reader.GetString("NM_PRODUTO");
                novoEstoque.IdFornecedor = reader.GetInt32("ID_FORNECEDOR");
                novoEstoque.Fornecedor = reader.GetString("NM_FORNECEDOR");


                estoque.Add(novoEstoque);
            }
            reader.Close();

            return estoque;
        }
        public void RemoverItem (EstoqueDTO fornecedor)
        {
            string script =
            @"UPDATE TB_ESTOQUE  
                 SET DS_QUANTIDADE = @DS_QUANTIDADE - 1
                  WHERE ID_ESTOQUE = @ID_ESTOQUE"
                  ;



            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_ESTOQUE", fornecedor.Id));
            parms.Add(new MySqlParameter("DS_QUANTIDADE", fornecedor.Quantidade));
           
           


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void AdicionarItem(int qtd, int idproduto,DateTime data)
        {
            string script =
            @"UPDATE TB_ESTOQUE 
                 SET DS_QUANTIDADE = DS_QUANTIDADE + @DS_QUANTIDADE,
                     DT_ULTIMA     = @DT_ULTIMA
                  WHERE ID_PRODUTO = @ID_PRODUTO"
                  ;



            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_QUANTIDADE", qtd));
           
            parms.Add(new MySqlParameter("ID_PRODUTO", idproduto));
            parms.Add(new MySqlParameter("DT_ULTIMA", data));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public EstoqueDTO ConsultarNome(int id)
        {
            string script = @"SELECT * FROM TB_ESTOQUE WHERE ID_PRODUTO = @ID_PRODUTO";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PRODUTO", id));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            EstoqueDTO dto = null;
            if (reader.Read())
            {
                dto = new EstoqueDTO();

                dto.Id = reader.GetInt32("ID_ESTOQUE");
                dto.Produto = reader.GetString("NM_PRODUTO");
                dto.IdProduto = reader.GetInt32("ID_PRODUTO");
            }
            reader.Close();

            return dto;
        }
    }
}

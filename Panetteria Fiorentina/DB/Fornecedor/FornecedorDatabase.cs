﻿using MySql.Data.MySqlClient;
using Panetteria_Fiorentina.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panetteria_Fiorentina.DB.Fornecedor
{
    class FornecedorDatabase
    {
        
        
        public int Salvar(FornecedorDTO fornecedor)
            {
                string script =
                @"INSERT INTO TB_FORNECEDOR
            (
	            NM_NOME,                                    
                DS_CNPJ,
                DS_ENDERECO,
                DS_NUMERO,
                DS_CIDADE,
                DS_CEP,
                DS_TELEFONE,
                DS_EMAIL,
                DT_CADASTRO,
                DS_OBSERVACAO
            )
            VALUES
            (
	            @NM_NOME,                                    
                @DS_CNPJ,
                @DS_ENDERECO,
                @DS_NUMERO,
                @DS_CIDADE,
                @DS_CEP,
                @DS_TELEDONE,
                @DS_EMAIL,
                @DT_CADASTRO,
                @DS_OBSERVACAO
            )";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("NM_NOME", fornecedor.Nome));
                parms.Add(new MySqlParameter("DS_CNPJ", fornecedor.CNPJ));
                parms.Add(new MySqlParameter("DS_ENDERECO", fornecedor.Endereco));
                parms.Add(new MySqlParameter("DS_NUMERO", fornecedor.Numero));
                parms.Add(new MySqlParameter("DS_CEP", fornecedor.CEP));
                parms.Add(new MySqlParameter("DS_TELEDONE", fornecedor.Telefone));
                parms.Add(new MySqlParameter("DS_CIDADE", fornecedor.Cidade));
                parms.Add(new MySqlParameter("DS_EMAIL", fornecedor.Email));
                parms.Add(new MySqlParameter("DT_CADASTRO", fornecedor.Cadastro));
                parms.Add(new MySqlParameter("DS_OBSERVACAO", fornecedor.Observacao));

            Database db = new Database();
                int pk = db.ExecuteInsertScriptWithPk(script, parms);
                return pk;
            }

            public void Alterar(FornecedorDTO fornecedor)
            {
            string script =
            @"UPDATE TB_FORNECEDOR 
                 SET  NM_NOME        = @NM_NOME,                                    
                      DS_CNPJ        = @DS_CNPJ,
                      DS_ENDERECO    = @DS_ENDERECO,
                      DS_NUMERO      = @DS_NUMERO,
                      DS_CIDADE      = @DS_CIDADE,
                      DS_CEP         = @DS_CEP,
                      DS_TELEFONE    = @DS_TELEFONE,
                      DS_EMAIL       = @DS_EMAIL,
                     
                      DS_OBSERVACAO  = @DS_OBSERVACAO ;";
                
                  
                
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("ID_FORNECEDOR", fornecedor.Id));
                parms.Add(new MySqlParameter("NM_NOME", fornecedor.Nome));
                parms.Add(new MySqlParameter("DS_CNPJ", fornecedor.CNPJ));
                parms.Add(new MySqlParameter("DS_ENDERECO", fornecedor.Endereco));
                parms.Add(new MySqlParameter("DS_NUMERO", fornecedor.Numero));
                parms.Add(new MySqlParameter("DS_CEP", fornecedor.CEP));
                parms.Add(new MySqlParameter("DS_TELEFONE", fornecedor.Telefone));
                parms.Add(new MySqlParameter("DS_CIDADE", fornecedor.Cidade));
               
                parms.Add(new MySqlParameter("DS_EMAIL", fornecedor.Email));
                parms.Add(new MySqlParameter("DS_OBSERVACAO", fornecedor.Observacao));


            Database db = new Database();
                db.ExecuteInsertScript(script, parms);
            }

            public void Remover(int id)
            {
                string script =
                @"DELETE FROM TB_FORNECEDOR WHERE ID_FORNECEDOR = @ID_FORNECEDOR";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("ID_FORNECEDOR", id));

                Database db = new Database();
                db.ExecuteInsertScript(script, parms);
            }

            public List<FornecedorDTO> Consultar()
            {
                string script =
                @"SELECT * 
                FROM TB_FORNECEDOR
                WHERE ID_FORNECEDOR > 1
               ;";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                

                Database db = new Database();
                MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

                List<FornecedorDTO> cliente = new List<FornecedorDTO>();

                while (reader.Read())
                {
                    FornecedorDTO novoFornecedor = new FornecedorDTO();
                    novoFornecedor.Id = reader.GetInt32("ID_FORNECEDOR");
                    novoFornecedor.Nome = reader.GetString("NM_NOME");
                    novoFornecedor.CNPJ = reader.GetString("DS_CNPJ");
                    novoFornecedor.Endereco = reader.GetString("DS_ENDERECO");
                    novoFornecedor.Numero = reader.GetString("DS_NUMERO");
                    novoFornecedor.CEP = reader.GetString("DS_CEP");
                    novoFornecedor.Telefone = reader.GetString("DS_TELEFONE");
                    novoFornecedor.Cidade = reader.GetString("DS_CIDADE");
                    novoFornecedor.Cadastro = reader.GetDateTime("DT_CADASTRO");
                    novoFornecedor.Email = reader.GetString("DS_EMAIL");
                    novoFornecedor.Observacao = reader.GetString("DS_OBSERVACAO");


                cliente.Add(novoFornecedor);
                }
                reader.Close();

                return cliente;
            }

        public List<FornecedorDTO> Listar()
        {
            string script = @"SELECT * FROM TB_FORNECEDOR
                               WHERE ID_FORNECEDOR > 1";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {

                FornecedorDTO novoFornecedor = new FornecedorDTO();
                novoFornecedor.Id = reader.GetInt32("ID_FORNECEDOR");
                novoFornecedor.Nome = reader.GetString("NM_NOME");
                novoFornecedor.CNPJ = reader.GetString("DS_CNPJ");
                novoFornecedor.Endereco = reader.GetString("DS_ENDERECO");
                novoFornecedor.Numero = reader.GetString("DS_NUMERO");
                novoFornecedor.CEP = reader.GetString("DS_CEP");
                novoFornecedor.Telefone = reader.GetString("DS_TELEFONE");
                novoFornecedor.Cidade = reader.GetString("DS_CIDADE");
                novoFornecedor.Cadastro = reader.GetDateTime("DT_CADASTRO");
                novoFornecedor.Email = reader.GetString("DS_EMAIL");
                novoFornecedor.Observacao = reader.GetString("DS_OBSERVACAO");


                lista.Add(novoFornecedor);
            }
            reader.Close();

            return lista;



        }
        public FornecedorDTO ConsultarPorCNPJ(string CNPJ)
        {
            string script = @"SELECT * FROM TB_FORNECEDOR WHERE DS_CNPJ = @DS_CNPJ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("DS_CNPJ", CNPJ));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FornecedorDTO dto = null;
            if (reader.Read())
            {
                dto = new FornecedorDTO();

                dto.Id = reader.GetInt32("ID_FUNCIONARIO");
                dto.Nome = reader.GetString("NM_NOME");
                dto.CNPJ = reader.GetString("DS_CNPJ");
            }
            reader.Close();

            return dto;
        }

    }
}

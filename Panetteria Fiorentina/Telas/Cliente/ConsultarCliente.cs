﻿using Panetteria_Fiorentina.DB.Cliente;
using Panetteria_Fiorentina.Telas.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class ConsultarCliente : Form
    {
        public ConsultarCliente()
        {
            InitializeComponent();
        }
        public void CarregarGrid()
        {
            string nome = txtNome.Text;
            ClienteBusiness Business = new ClienteBusiness();
            List<ClienteDTO> lista = Business.Consultar(nome);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }



        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }


        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }


        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                CarregarGrid();
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possivel realizar a consulta: " + ex.Message);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                ClienteDTO funcionario = dataGridView1.CurrentRow.DataBoundItem as ClienteDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o cliente?", "Panetteria Fiorentino",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ClienteBusiness business = new ClienteBusiness();
                    business.Remover(funcionario.ID);

                    CarregarGrid();
                }
            }
            if (e.ColumnIndex == 0)
            {
                ClienteDTO funcionario = dataGridView1.CurrentRow.DataBoundItem as ClienteDTO;

                DialogResult r = MessageBox.Show("Deseja alterar o cliente?", "Panetteria Fiorentino",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ClienteDTO dto = dataGridView1.Rows[e.RowIndex].DataBoundItem as ClienteDTO;
                    this.Close();
                    AlterarCliente tela = new AlterarCliente();
                    tela.LoadScrean(dto);
                    tela.ShowDialog();
                }
            }
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 1)
                {
                    ClienteDTO funcionario = dataGridView1.CurrentRow.DataBoundItem as ClienteDTO;

                    DialogResult r = MessageBox.Show("Deseja excluir o cliente?", "Panetteria Fiorentino",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        ClienteBusiness business = new ClienteBusiness();
                        business.Remover(funcionario.ID);

                        CarregarGrid();
                    }
                }
                if (e.ColumnIndex == 0)
                {
                    ClienteDTO funcionario = dataGridView1.CurrentRow.DataBoundItem as ClienteDTO;

                    DialogResult r = MessageBox.Show("Deseja alterar o cliente?", "Panetteria Fiorentino",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        ClienteDTO dto = dataGridView1.Rows[e.RowIndex].DataBoundItem as ClienteDTO;
                        this.Close();
                        AlterarCliente tela = new AlterarCliente();
                        tela.LoadScrean(dto);
                        tela.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {

                EnviarMensagemErro("Não foi possível deletar o cliente: " + ex.Message);
            }
           
        }
    }
}

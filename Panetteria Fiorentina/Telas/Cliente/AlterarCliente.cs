﻿using Newtonsoft.Json;
using Panetteria_Fiorentina.DB.Cliente;
using Panetteria_Fiorentina.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina.Telas.Cliente
{
    public partial class AlterarCliente : Form
    {
        public AlterarCliente()
        {
            InitializeComponent();
        }
        ClienteDTO dto;
        public void LoadScrean(ClienteDTO dto)
        {
            this.dto = dto;

            txtNome.Text = dto.Nome;
            RGtext.Text = dto.RG;
            txtCPF.Text = dto.CPF;
            txtCelular.Text = dto.Celular;
            txtEmail.Text = dto.Email;
            dtpNascimento.Value = dto.Nascimento;
            txtCep.Text = dto.CEP;
            txtEndereco.Text = dto.Endereço;
            txtNumero.Text = dto.Numero;
            txtComplemento.Text = dto.Complemento;
            txtTelefone.Text = dto.Telefone;
            textBox1.Text = dto.Observacao;

        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void label6_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
            tela.CarregarGrid();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {

                this.dto.Nome = txtNome.Text;
                this.dto.RG = RGtext.Text;
                this.dto.CPF = txtCPF.Text;
                this.dto.Celular = txtCelular.Text;
                this.dto.Email = txtEmail.Text;
                this.dto.Nascimento = dtpNascimento.Value.Date;
                this.dto.CEP = txtCep.Text;
                this.dto.Endereço = txtEndereco.Text;
                this.dto.Numero = txtNumero.Text;
                this.dto.Complemento = txtComplemento.Text;
                this.dto.Telefone = txtTelefone.Text;
                this.dto.Observacao = textBox1.Text;


                ClienteBusiness buss = new ClienteBusiness();
                buss.Alterar(dto);

                EnviarMensagem("Cliente alterado com sucesso.");
                this.Close();
                ConsultarCliente tela = new ConsultarCliente();
                tela.Show();
                tela.CarregarGrid();
            }
            catch(Exception ex)
            {
                EnviarMensagemErro("Houve um erro ao alterar o cliente: " + ex.Message);
            }
        }

        private void txtEndereco_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void btnBuscarCEP_Click(object sender, EventArgs e)
        {
            string cep = txtCep.Text.Trim().Replace("-", "");

            CorreioResponseFunc correio = BuscarAPICorreio(cep);

            txtEndereco.Text = correio.Logradouro;

        }
        private CorreioResponseFunc BuscarAPICorreio(string cep)
        {

            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;


            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");


            CorreioResponseFunc correio = JsonConvert.DeserializeObject<CorreioResponseFunc>(resposta);
            return correio;

        }
    }
    
}

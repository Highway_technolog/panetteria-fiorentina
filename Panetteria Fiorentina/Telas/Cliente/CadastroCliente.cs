﻿using Newtonsoft.Json;
using Panetteria_Fiorentina.DB.Cliente;
using Panetteria_Fiorentina.DB.Fornecedor;
using Panetteria_Fiorentina.DB.Funcionario;
using Panetteria_Fiorentina.DB.PLUGIN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class CadastroCliente : Form
    {
        public CadastroCliente()
        {
            InitializeComponent();
        }

        public void Limpar()
        {
            txtNome.Text = "";
            txtCPF.Text = "";
            txtCelular.Text = "";
            txtCep.Text = "";
            txtComplemento.Text = "";
            txtEmail.Text = "";
            txtEndereco.Text = "";
            txtNumero.Text = "";
            txtTelefone.Text = "";


        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Nome = txtNome.Text;
                dto.RG = RGtext.Text;
                dto.CPF = txtCPF.Text;
                dto.Celular = txtCelular.Text;
                dto.Email = txtEmail.Text;
                dto.Nascimento = dtpNascimento.Value.Date;
                dto.CEP = txtCep.Text;
                dto.Endereço = txtEndereco.Text;
                dto.Numero = txtNumero.Text;
                dto.Complemento = txtComplemento.Text;
                dto.Telefone = txtTelefone.Text;
                dto.Observacao = textBox1.Text;

                ClienteBusiness buss = new ClienteBusiness();
                buss.Salvar(dto);

               
            

                EnviarMensagem("Cliente cadastrado com sucesso");

                try
                {

                    string conteudo = "Cadastro foi efetuado com sucesso";
                    Email email = new Email();
                    email.Enviar(txtEmail.Text, conteudo);
                }
                catch (Exception ex)
                {

                    EnviarMensagemErro("Não foi possível cadastrar o cliente: " + ex.Message);
                }

                EnviarMensagemNovo("Deseja cadastrar um novo cliente ?");

                
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possível cadastrar o cliente: " + ex.Message);
            }
           

        }
        private void button4_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private void RGtext_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void btnBuscarCEP_Click(object sender, EventArgs e)
        {
             string cep = txtCep.Text.Trim().Replace("-", "");

                CorreioResponseFunc correio = BuscarAPICorreio(cep);

                txtEndereco.Text = correio.Logradouro;
            
        }
        private CorreioResponseFunc BuscarAPICorreio(string cep)
        {

            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;


            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");


            CorreioResponseFunc correio = JsonConvert.DeserializeObject<CorreioResponseFunc>(resposta);
            return correio;

        }

        private void txtEmail_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}

    

﻿using Panetteria_Fiorentina.DB.Funcionario;
using Panetteria_Fiorentina.PluginIBM;
using Panetteria_Fiorentina.Telas.Compra;
using Panetteria_Fiorentina.Telas.Fluxo_de_caixa;
using Panetteria_Fiorentina.Telas.Venda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();
            VerificarPermissoes();
            MudarNome();
            
        }
        private void MudarNome()
        {
            string usuario = UserSession.UsuarioLogado.Usuario;
            lblus.Text = usuario;
        }

        void VerificarPermissoes()
        {
            if (UserSession.UsuarioLogado.Admin == false)
           {
                if (UserSession.UsuarioLogado.RH == true)
                {
                   CMSFuncionario.Enabled = true;
                }

                if (UserSession.UsuarioLogado.Logistica == true)
                {
                    CMSCliente.Enabled = true;
                }
                if (UserSession.UsuarioLogado.Financeiro == true)
                {
                    CMSFolhaPagamento.Enabled = true;
                    CMSFornecedor.Enabled = true;
                    CMSFolhaPagamento.Enabled = true;
                }
               if (UserSession.UsuarioLogado.Vendas == true)
                {
                    CMSCliente.Enabled = true;
                    CMSProduto.Enabled = true;
                    CMSPedidoVenda.Enabled = true;
                }
                if (UserSession.UsuarioLogado.Compras == true)
                {
                    CMSFornecedor.Enabled = true;
                }
                if (UserSession.UsuarioLogado.Empregado == true)
                {
                    CMSProduto.Enabled = true;
                    
                }
            }
        }
        private void pictureBox11_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Menu_Load(object sender, EventArgs e)
        {
            
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void CMSFolhaPagamento_Opening(object sender, CancelEventArgs e)
        {

        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label10_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void ptbProduto_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void CMSProduto_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
           
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.Hide();
            FluxoCaixa a = new FluxoCaixa();
            a.Show();
        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            CadastroCliente tela = new CadastroCliente();
            tela.Show();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
        }

        private void novoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
          
           
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            
           
        }

        private void novoToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            
             this.Close();
             CadastrarFonecedor tela = new CadastrarFonecedor();
             tela.Show();
        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
          
            this.Close();
            ConsultarFornecedor tela = new ConsultarFornecedor();
            tela.Show();
        }

        private void novoToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            this.Hide();
            CadastroFuncionario tela = new CadastroFuncionario();
            tela.Show();
        }

        private void consultarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConsultarFuncionario tela = new ConsultarFuncionario();
            tela.Show();
        }

        private void novoToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox11_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void ptbCliente_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void ptbFolhaPagamento_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void ptbFornecedor_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void label3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {

            DialogResult r = MessageBox.Show("Realmente deseja deslogar do sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Information);

            if (r == DialogResult.Yes)
            {
                this.Hide();
                Login tela = new Login();
                tela.Show();
            }
            
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aperte com o botão direito para mais opções", "Panetteria Fiorentina",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Information);
        }

        private void CMSPedidoVenda_Opening(object sender, CancelEventArgs e)
        {

        }

        private void novaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            PedidoCompra tela = new PedidoCompra();
            tela.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConsultaFolhaPagamento tela = new ConsultaFolhaPagamento();
            tela.Show();
        }

        private void novoToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            this.Hide();
            FolhaPagamento tela = new FolhaPagamento();
            tela.Show();
        }

        private void consultarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            ConsultarCompra a = new ConsultarCompra();
            a.Show();
            this.Hide();
        }

        private void novaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PedidoVenda a = new PedidoVenda();
            a.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            ConsultarVenda a = new ConsultarVenda();
            a.Show();
            this.Hide();
        }

        private void novoToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            Producao tela = new Producao();
            tela.Show();
            this.Close();
        }

        private void consultarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            ConsultarProducao tela = new ConsultarProducao();
            tela.Show();
            this.Close();
        }

        private void novoToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            this.Hide();
            CadastrarProduto tela = new CadastrarProduto();
            tela.Show();
        }

        private void consultarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConsultarProduto tela = new ConsultarProduto();
            tela.Show();
        }

        private void cadastrarGastoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Gastos a = new Gastos();
            a.Show();
        }

        private void consultarGastoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConsultarGastos a = new ConsultarGastos();
            a.Show();
        }

        private void estoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Estoque a = new Estoque();
            a.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            asd tela = new asd();
            tela.Show();
        }

        IbmVoiceApi ibmApi = new IbmVoiceApi();

        private void btnFalar_Click(object sender, EventArgs e)
        {
            ibmApi.IniciarOuvir();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
            asd tela = new asd();
            tela.Show();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            this.Hide();
            asd tela = new asd();
            tela.Show();
        }

        private void CMSFolhaPagamento_Opening_1(object sender, CancelEventArgs e)
        {

        }

        private void pedidosPendentesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            PedidosPendentes tela = new PedidosPendentes();
            tela.Show();
        }

        private void btnFalar_Click_1(object sender, EventArgs e)
        {
            ibmApi.IniciarOuvir();
        }

        private void btnParar_Click(object sender, EventArgs e)
        {
            string texto = ibmApi.PararOuvir();
            lblFalar.Text = texto;
            CarregarTela();
        }
        void CarregarTela()
        {
            if (lblFalar.Text.Contains("folha de pagamento"))
            {
                this.Hide();
                FolhaPagamento tela = new FolhaPagamento();
                tela.Show();
            }
            else if (lblFalar.Text.Contains("cadastrar cliente"))
            {
                this.Hide();
                CadastroCliente a = new CadastroCliente();
                a.Show();
            }

            else if (lblFalar.Text.Contains("consultar cliente"))
            {
                this.Hide();
                ConsultarCliente c = new ConsultarCliente();
                c.Show();
            }
            else if (lblFalar.Text.Contains("consultar compra"))
            {
                this.Hide();
                ConsultarCompra b = new ConsultarCompra();
                b.Show();
            }
            else if (lblFalar.Text.Contains("pedir compra"))
            {
                this.Hide();
                FolhaPagamento d = new FolhaPagamento();
                d.Show();
            }
            else if (lblFalar.Text.Contains("consultar gastos"))
            {
                this.Hide();
                ConsultarGastos e = new ConsultarGastos();
                e.Show();
            }
            else if (lblFalar.Text.Contains("novo gasto"))
            {
                this.Hide();
                Gastos f = new Gastos();
                f.Show();
            }

            else if (lblFalar.Text.Contains("cadastrar fornecedor"))
            {
                this.Hide();
                CadastrarFonecedor g = new CadastrarFonecedor();
                g.Show();
            }
            else if (lblFalar.Text.Contains("consultar fornecedor"))
            {
                this.Hide();
                ConsultarFornecedor h = new ConsultarFornecedor();
                h.Show();
            }
            else if (lblFalar.Text.Contains("cadastrar funcionário"))
            {
                this.Hide();
                CadastroFuncionario i = new CadastroFuncionario();
                i.Show();
            }
            else if (lblFalar.Text.Contains("consultar folha de pagamento"))
            {
                this.Hide();
                ConsultaFolhaPagamento j = new ConsultaFolhaPagamento();
                j.Show();
            }
            else if (lblFalar.Text.Contains("cadastrar produto"))
            {
                this.Hide();
                CadastrarProduto k = new CadastrarProduto();
                k.Show();
            }
            else if (lblFalar.Text.Contains("consultar produto"))
            {
                this.Hide();
                ConsultarProduto l = new ConsultarProduto();
                l.Show();
            }
            else if (lblFalar.Text.Contains("estoque"))
            {
                this.Hide();
                Estoque m = new Estoque();
                m.Show();
            }
            else if (lblFalar.Text.Contains("consultar produção"))
            {
                this.Hide();
                ConsultarProducao n = new ConsultarProducao();
                n.Show();
            }
            else if (lblFalar.Text.Contains("consultar venda"))
            {
                this.Hide();
                ConsultarVenda o = new ConsultarVenda();
                o.Show();
            }
            else if (lblFalar.Text.Contains("pedidos pendentes"))
            {
                this.Hide();
                PedidosPendentes p = new PedidosPendentes();
                p.Show();
            }
            else if (lblFalar.Text.Contains("nova venda"))
            {
                this.Hide();
                PedidoVenda q = new PedidoVenda();
                q.Show();
            }
            else if (lblFalar.Text.Contains("cadastrar produção"))
            {
                this.Hide();
                Producao r = new Producao();
                r.Show();
            }
            else
            {
                MessageBox.Show("Não foi possivel identificar identificar a tela requerida.", "Panetteria Fiorentina",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                
            }
        }


        private void label7_Click_1(object sender, EventArgs e)
        {

        }
    }
}

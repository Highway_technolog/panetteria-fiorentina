﻿namespace Panetteria_Fiorentina
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuPrincipal));
            this.label10 = new System.Windows.Forms.Label();
            this.ptbFornecedor = new System.Windows.Forms.PictureBox();
            this.CMSFornecedor = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.novoToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.lblus = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ptbProduto = new System.Windows.Forms.PictureBox();
            this.CMSProduto = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.novoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.novoToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.novoToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.estoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.CMSFuncionario = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.novoToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.CMSPedidoVenda = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.novoToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.novaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidosPendentesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.novaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.ptbFolhaPagamento = new System.Windows.Forms.PictureBox();
            this.CMSFolhaPagamento = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.novoToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.gastosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarGastoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarGastoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ptbCliente = new System.Windows.Forms.PictureBox();
            this.CMSCliente = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.novoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label22 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnParar = new System.Windows.Forms.Button();
            this.btnFalar = new System.Windows.Forms.Button();
            this.lblFalar = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ptbFornecedor)).BeginInit();
            this.CMSFornecedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbProduto)).BeginInit();
            this.CMSProduto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.CMSFuncionario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.CMSPedidoVenda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbFolhaPagamento)).BeginInit();
            this.CMSFolhaPagamento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptbCliente)).BeginInit();
            this.CMSCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(632, 315);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 18);
            this.label10.TabIndex = 210;
            this.label10.Text = "Fornecedor";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // ptbFornecedor
            // 
            this.ptbFornecedor.BackColor = System.Drawing.Color.Transparent;
            this.ptbFornecedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbFornecedor.BackgroundImage")));
            this.ptbFornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbFornecedor.ContextMenuStrip = this.CMSFornecedor;
            this.ptbFornecedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptbFornecedor.Location = new System.Drawing.Point(632, 241);
            this.ptbFornecedor.Name = "ptbFornecedor";
            this.ptbFornecedor.Size = new System.Drawing.Size(98, 71);
            this.ptbFornecedor.TabIndex = 209;
            this.ptbFornecedor.TabStop = false;
            this.ptbFornecedor.Click += new System.EventHandler(this.ptbFornecedor_Click);
            // 
            // CMSFornecedor
            // 
            this.CMSFornecedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSFornecedor.BackgroundImage")));
            this.CMSFornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSFornecedor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem2,
            this.consultarToolStripMenuItem3});
            this.CMSFornecedor.Name = "CMSProduto";
            this.CMSFornecedor.Size = new System.Drawing.Size(126, 48);
            // 
            // novoToolStripMenuItem2
            // 
            this.novoToolStripMenuItem2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem2.BackgroundImage")));
            this.novoToolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem2.Image")));
            this.novoToolStripMenuItem2.Name = "novoToolStripMenuItem2";
            this.novoToolStripMenuItem2.Size = new System.Drawing.Size(125, 22);
            this.novoToolStripMenuItem2.Text = "Novo";
            this.novoToolStripMenuItem2.Click += new System.EventHandler(this.novoToolStripMenuItem2_Click);
            // 
            // consultarToolStripMenuItem3
            // 
            this.consultarToolStripMenuItem3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem3.BackgroundImage")));
            this.consultarToolStripMenuItem3.Image = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem3.Image")));
            this.consultarToolStripMenuItem3.Name = "consultarToolStripMenuItem3";
            this.consultarToolStripMenuItem3.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem3.Text = "Consultar";
            this.consultarToolStripMenuItem3.Click += new System.EventHandler(this.consultarToolStripMenuItem3_Click);
            // 
            // lblus
            // 
            this.lblus.AutoSize = true;
            this.lblus.BackColor = System.Drawing.Color.Black;
            this.lblus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblus.ForeColor = System.Drawing.Color.White;
            this.lblus.Location = new System.Drawing.Point(73, 29);
            this.lblus.Name = "lblus";
            this.lblus.Size = new System.Drawing.Size(64, 20);
            this.lblus.TabIndex = 208;
            this.lblus.Text = "Usuário";
            this.lblus.Click += new System.EventHandler(this.label7_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Black;
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.ContextMenuStrip = this.contextMenuStrip1;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Location = new System.Drawing.Point(12, 8);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(55, 50);
            this.pictureBox3.TabIndex = 207;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("contextMenuStrip1.BackgroundImage")));
            this.contextMenuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3});
            this.contextMenuStrip1.Name = "CMSProduto";
            this.contextMenuStrip1.Size = new System.Drawing.Size(113, 26);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem3.BackgroundImage")));
            this.toolStripMenuItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem3.Image")));
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(112, 22);
            this.toolStripMenuItem3.Text = "Logout";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(596, 433);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 16);
            this.label6.TabIndex = 206;
            this.label6.Text = "Produto/Produção";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(45, 315);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 18);
            this.label5.TabIndex = 205;
            this.label5.Text = "Finanças";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(20, 437);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 18);
            this.label4.TabIndex = 204;
            this.label4.Text = "Venda / Compra";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(631, 207);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 18);
            this.label3.TabIndex = 203;
            this.label3.Text = "Funcionario";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(51, 207);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 18);
            this.label2.TabIndex = 202;
            this.label2.Text = "Cliente";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // ptbProduto
            // 
            this.ptbProduto.BackColor = System.Drawing.Color.Transparent;
            this.ptbProduto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbProduto.BackgroundImage")));
            this.ptbProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbProduto.ContextMenuStrip = this.CMSProduto;
            this.ptbProduto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptbProduto.Location = new System.Drawing.Point(632, 359);
            this.ptbProduto.Name = "ptbProduto";
            this.ptbProduto.Size = new System.Drawing.Size(98, 71);
            this.ptbProduto.TabIndex = 201;
            this.ptbProduto.TabStop = false;
            this.ptbProduto.Click += new System.EventHandler(this.ptbProduto_Click);
            // 
            // CMSProduto
            // 
            this.CMSProduto.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSProduto.BackgroundImage")));
            this.CMSProduto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSProduto.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem1,
            this.consultarToolStripMenuItem2,
            this.estoqueToolStripMenuItem});
            this.CMSProduto.Name = "CMSProduto";
            this.CMSProduto.Size = new System.Drawing.Size(126, 70);
            this.CMSProduto.Opening += new System.ComponentModel.CancelEventHandler(this.CMSProduto_Opening);
            // 
            // novoToolStripMenuItem1
            // 
            this.novoToolStripMenuItem1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem1.BackgroundImage")));
            this.novoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem6,
            this.consultarToolStripMenuItem8});
            this.novoToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem1.Image")));
            this.novoToolStripMenuItem1.Name = "novoToolStripMenuItem1";
            this.novoToolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.novoToolStripMenuItem1.Text = "Produto";
            this.novoToolStripMenuItem1.Click += new System.EventHandler(this.novoToolStripMenuItem1_Click);
            // 
            // novoToolStripMenuItem6
            // 
            this.novoToolStripMenuItem6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem6.BackgroundImage")));
            this.novoToolStripMenuItem6.Image = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem6.Image")));
            this.novoToolStripMenuItem6.Name = "novoToolStripMenuItem6";
            this.novoToolStripMenuItem6.Size = new System.Drawing.Size(125, 22);
            this.novoToolStripMenuItem6.Text = "Novo";
            this.novoToolStripMenuItem6.Click += new System.EventHandler(this.novoToolStripMenuItem6_Click);
            // 
            // consultarToolStripMenuItem8
            // 
            this.consultarToolStripMenuItem8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem8.BackgroundImage")));
            this.consultarToolStripMenuItem8.Image = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem8.Image")));
            this.consultarToolStripMenuItem8.Name = "consultarToolStripMenuItem8";
            this.consultarToolStripMenuItem8.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem8.Text = "Consultar";
            this.consultarToolStripMenuItem8.Click += new System.EventHandler(this.consultarToolStripMenuItem8_Click);
            // 
            // consultarToolStripMenuItem2
            // 
            this.consultarToolStripMenuItem2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem2.BackgroundImage")));
            this.consultarToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem7,
            this.consultarToolStripMenuItem9});
            this.consultarToolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem2.Image")));
            this.consultarToolStripMenuItem2.Name = "consultarToolStripMenuItem2";
            this.consultarToolStripMenuItem2.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem2.Text = "Produção";
            this.consultarToolStripMenuItem2.Click += new System.EventHandler(this.consultarToolStripMenuItem2_Click);
            // 
            // novoToolStripMenuItem7
            // 
            this.novoToolStripMenuItem7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem7.BackgroundImage")));
            this.novoToolStripMenuItem7.Image = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem7.Image")));
            this.novoToolStripMenuItem7.Name = "novoToolStripMenuItem7";
            this.novoToolStripMenuItem7.Size = new System.Drawing.Size(125, 22);
            this.novoToolStripMenuItem7.Text = "Novo";
            this.novoToolStripMenuItem7.Click += new System.EventHandler(this.novoToolStripMenuItem7_Click);
            // 
            // consultarToolStripMenuItem9
            // 
            this.consultarToolStripMenuItem9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem9.BackgroundImage")));
            this.consultarToolStripMenuItem9.Image = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem9.Image")));
            this.consultarToolStripMenuItem9.Name = "consultarToolStripMenuItem9";
            this.consultarToolStripMenuItem9.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem9.Text = "Consultar";
            this.consultarToolStripMenuItem9.Click += new System.EventHandler(this.consultarToolStripMenuItem9_Click);
            // 
            // estoqueToolStripMenuItem
            // 
            this.estoqueToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("estoqueToolStripMenuItem.BackgroundImage")));
            this.estoqueToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("estoqueToolStripMenuItem.Image")));
            this.estoqueToolStripMenuItem.Name = "estoqueToolStripMenuItem";
            this.estoqueToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.estoqueToolStripMenuItem.Text = "Estoque";
            this.estoqueToolStripMenuItem.Click += new System.EventHandler(this.estoqueToolStripMenuItem_Click);
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox11.BackgroundImage")));
            this.pictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox11.ContextMenuStrip = this.CMSFuncionario;
            this.pictureBox11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox11.Location = new System.Drawing.Point(630, 133);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(100, 71);
            this.pictureBox11.TabIndex = 200;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Click += new System.EventHandler(this.pictureBox11_Click_1);
            // 
            // CMSFuncionario
            // 
            this.CMSFuncionario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSFuncionario.BackgroundImage")));
            this.CMSFuncionario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSFuncionario.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem3,
            this.consultarToolStripMenuItem4});
            this.CMSFuncionario.Name = "CMSProduto";
            this.CMSFuncionario.Size = new System.Drawing.Size(126, 48);
            // 
            // novoToolStripMenuItem3
            // 
            this.novoToolStripMenuItem3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem3.BackgroundImage")));
            this.novoToolStripMenuItem3.Image = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem3.Image")));
            this.novoToolStripMenuItem3.Name = "novoToolStripMenuItem3";
            this.novoToolStripMenuItem3.Size = new System.Drawing.Size(125, 22);
            this.novoToolStripMenuItem3.Text = "Novo";
            this.novoToolStripMenuItem3.Click += new System.EventHandler(this.novoToolStripMenuItem3_Click);
            // 
            // consultarToolStripMenuItem4
            // 
            this.consultarToolStripMenuItem4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem4.BackgroundImage")));
            this.consultarToolStripMenuItem4.Image = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem4.Image")));
            this.consultarToolStripMenuItem4.Name = "consultarToolStripMenuItem4";
            this.consultarToolStripMenuItem4.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem4.Text = "Consultar";
            this.consultarToolStripMenuItem4.Click += new System.EventHandler(this.consultarToolStripMenuItem4_Click);
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox10.BackgroundImage")));
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox10.ContextMenuStrip = this.CMSPedidoVenda;
            this.pictureBox10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox10.Location = new System.Drawing.Point(38, 356);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(98, 71);
            this.pictureBox10.TabIndex = 199;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Click += new System.EventHandler(this.pictureBox10_Click);
            // 
            // CMSPedidoVenda
            // 
            this.CMSPedidoVenda.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSPedidoVenda.BackgroundImage")));
            this.CMSPedidoVenda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSPedidoVenda.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem4,
            this.consultarToolStripMenuItem5});
            this.CMSPedidoVenda.Name = "CMSProduto";
            this.CMSPedidoVenda.Size = new System.Drawing.Size(118, 48);
            this.CMSPedidoVenda.Opening += new System.ComponentModel.CancelEventHandler(this.CMSPedidoVenda_Opening);
            // 
            // novoToolStripMenuItem4
            // 
            this.novoToolStripMenuItem4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem4.BackgroundImage")));
            this.novoToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novaToolStripMenuItem,
            this.consultarToolStripMenuItem6,
            this.pedidosPendentesToolStripMenuItem});
            this.novoToolStripMenuItem4.Image = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem4.Image")));
            this.novoToolStripMenuItem4.Name = "novoToolStripMenuItem4";
            this.novoToolStripMenuItem4.Size = new System.Drawing.Size(117, 22);
            this.novoToolStripMenuItem4.Text = "Venda";
            this.novoToolStripMenuItem4.Click += new System.EventHandler(this.novoToolStripMenuItem4_Click);
            // 
            // novaToolStripMenuItem
            // 
            this.novaToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("novaToolStripMenuItem.BackgroundImage")));
            this.novaToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.novaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("novaToolStripMenuItem.Image")));
            this.novaToolStripMenuItem.Name = "novaToolStripMenuItem";
            this.novaToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.novaToolStripMenuItem.Text = "Nova";
            this.novaToolStripMenuItem.Click += new System.EventHandler(this.novaToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem6
            // 
            this.consultarToolStripMenuItem6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem6.BackgroundImage")));
            this.consultarToolStripMenuItem6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.consultarToolStripMenuItem6.Image = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem6.Image")));
            this.consultarToolStripMenuItem6.Name = "consultarToolStripMenuItem6";
            this.consultarToolStripMenuItem6.Size = new System.Drawing.Size(174, 22);
            this.consultarToolStripMenuItem6.Text = "Consultar";
            this.consultarToolStripMenuItem6.Click += new System.EventHandler(this.consultarToolStripMenuItem6_Click);
            // 
            // pedidosPendentesToolStripMenuItem
            // 
            this.pedidosPendentesToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pedidosPendentesToolStripMenuItem.BackgroundImage")));
            this.pedidosPendentesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pedidosPendentesToolStripMenuItem.Image")));
            this.pedidosPendentesToolStripMenuItem.Name = "pedidosPendentesToolStripMenuItem";
            this.pedidosPendentesToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.pedidosPendentesToolStripMenuItem.Text = "Pedidos Pendentes";
            this.pedidosPendentesToolStripMenuItem.Click += new System.EventHandler(this.pedidosPendentesToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem5
            // 
            this.consultarToolStripMenuItem5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem5.BackgroundImage")));
            this.consultarToolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novaToolStripMenuItem1,
            this.consultarToolStripMenuItem7});
            this.consultarToolStripMenuItem5.Image = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem5.Image")));
            this.consultarToolStripMenuItem5.Name = "consultarToolStripMenuItem5";
            this.consultarToolStripMenuItem5.Size = new System.Drawing.Size(117, 22);
            this.consultarToolStripMenuItem5.Text = "Compra";
            // 
            // novaToolStripMenuItem1
            // 
            this.novaToolStripMenuItem1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("novaToolStripMenuItem1.BackgroundImage")));
            this.novaToolStripMenuItem1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.novaToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("novaToolStripMenuItem1.Image")));
            this.novaToolStripMenuItem1.Name = "novaToolStripMenuItem1";
            this.novaToolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.novaToolStripMenuItem1.Text = "Nova";
            this.novaToolStripMenuItem1.Click += new System.EventHandler(this.novaToolStripMenuItem1_Click);
            // 
            // consultarToolStripMenuItem7
            // 
            this.consultarToolStripMenuItem7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem7.BackgroundImage")));
            this.consultarToolStripMenuItem7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.consultarToolStripMenuItem7.Image = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem7.Image")));
            this.consultarToolStripMenuItem7.Name = "consultarToolStripMenuItem7";
            this.consultarToolStripMenuItem7.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem7.Text = "Consultar";
            this.consultarToolStripMenuItem7.Click += new System.EventHandler(this.consultarToolStripMenuItem7_Click);
            // 
            // ptbFolhaPagamento
            // 
            this.ptbFolhaPagamento.BackColor = System.Drawing.Color.Transparent;
            this.ptbFolhaPagamento.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbFolhaPagamento.BackgroundImage")));
            this.ptbFolhaPagamento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbFolhaPagamento.ContextMenuStrip = this.CMSFolhaPagamento;
            this.ptbFolhaPagamento.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptbFolhaPagamento.Location = new System.Drawing.Point(36, 241);
            this.ptbFolhaPagamento.Name = "ptbFolhaPagamento";
            this.ptbFolhaPagamento.Size = new System.Drawing.Size(98, 71);
            this.ptbFolhaPagamento.TabIndex = 198;
            this.ptbFolhaPagamento.TabStop = false;
            this.ptbFolhaPagamento.Click += new System.EventHandler(this.ptbFolhaPagamento_Click);
            // 
            // CMSFolhaPagamento
            // 
            this.CMSFolhaPagamento.BackColor = System.Drawing.Color.Wheat;
            this.CMSFolhaPagamento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSFolhaPagamento.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.gastosToolStripMenuItem});
            this.CMSFolhaPagamento.Name = "CMSCliente";
            this.CMSFolhaPagamento.Size = new System.Drawing.Size(184, 70);
            this.CMSFolhaPagamento.Opening += new System.ComponentModel.CancelEventHandler(this.CMSFolhaPagamento_Opening_1);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.BackgroundImage")));
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem5,
            this.consultarToolStripMenuItem});
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(183, 22);
            this.toolStripMenuItem1.Text = "Folha de pagamento";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // novoToolStripMenuItem5
            // 
            this.novoToolStripMenuItem5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem5.BackgroundImage")));
            this.novoToolStripMenuItem5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.novoToolStripMenuItem5.Image = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem5.Image")));
            this.novoToolStripMenuItem5.Name = "novoToolStripMenuItem5";
            this.novoToolStripMenuItem5.Size = new System.Drawing.Size(125, 22);
            this.novoToolStripMenuItem5.Text = "Novo";
            this.novoToolStripMenuItem5.Click += new System.EventHandler(this.novoToolStripMenuItem5_Click);
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.BackColor = System.Drawing.Color.Wheat;
            this.consultarToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem.BackgroundImage")));
            this.consultarToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.consultarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem.Image")));
            this.consultarToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.DarkOliveGreen;
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem.Text = "Consultar";
            this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.BackgroundImage")));
            this.toolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.Image")));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(183, 22);
            this.toolStripMenuItem2.Text = "Fluxo de caixa";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // gastosToolStripMenuItem
            // 
            this.gastosToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gastosToolStripMenuItem.BackgroundImage")));
            this.gastosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarGastoToolStripMenuItem,
            this.consultarGastoToolStripMenuItem});
            this.gastosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("gastosToolStripMenuItem.Image")));
            this.gastosToolStripMenuItem.Name = "gastosToolStripMenuItem";
            this.gastosToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.gastosToolStripMenuItem.Text = "Gastos";
            // 
            // cadastrarGastoToolStripMenuItem
            // 
            this.cadastrarGastoToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cadastrarGastoToolStripMenuItem.BackgroundImage")));
            this.cadastrarGastoToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cadastrarGastoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cadastrarGastoToolStripMenuItem.Image")));
            this.cadastrarGastoToolStripMenuItem.Name = "cadastrarGastoToolStripMenuItem";
            this.cadastrarGastoToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.cadastrarGastoToolStripMenuItem.Text = "Cadastrar gasto";
            this.cadastrarGastoToolStripMenuItem.Click += new System.EventHandler(this.cadastrarGastoToolStripMenuItem_Click);
            // 
            // consultarGastoToolStripMenuItem
            // 
            this.consultarGastoToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarGastoToolStripMenuItem.BackgroundImage")));
            this.consultarGastoToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.consultarGastoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("consultarGastoToolStripMenuItem.Image")));
            this.consultarGastoToolStripMenuItem.Name = "consultarGastoToolStripMenuItem";
            this.consultarGastoToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.consultarGastoToolStripMenuItem.Text = "Consultar gasto";
            this.consultarGastoToolStripMenuItem.Click += new System.EventHandler(this.consultarGastoToolStripMenuItem_Click);
            // 
            // ptbCliente
            // 
            this.ptbCliente.BackColor = System.Drawing.Color.Transparent;
            this.ptbCliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ptbCliente.BackgroundImage")));
            this.ptbCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ptbCliente.ContextMenuStrip = this.CMSCliente;
            this.ptbCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ptbCliente.Location = new System.Drawing.Point(34, 133);
            this.ptbCliente.Name = "ptbCliente";
            this.ptbCliente.Size = new System.Drawing.Size(100, 71);
            this.ptbCliente.TabIndex = 197;
            this.ptbCliente.TabStop = false;
            this.ptbCliente.Click += new System.EventHandler(this.ptbCliente_Click);
            // 
            // CMSCliente
            // 
            this.CMSCliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("CMSCliente.BackgroundImage")));
            this.CMSCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CMSCliente.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem,
            this.consultarToolStripMenuItem1});
            this.CMSCliente.Name = "CMSCliente";
            this.CMSCliente.Size = new System.Drawing.Size(126, 48);
            // 
            // novoToolStripMenuItem
            // 
            this.novoToolStripMenuItem.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem.BackgroundImage")));
            this.novoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("novoToolStripMenuItem.Image")));
            this.novoToolStripMenuItem.Name = "novoToolStripMenuItem";
            this.novoToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.novoToolStripMenuItem.Text = "Novo";
            this.novoToolStripMenuItem.Click += new System.EventHandler(this.novoToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem1.BackgroundImage")));
            this.consultarToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("consultarToolStripMenuItem1.Image")));
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem1.Text = "Consultar";
            this.consultarToolStripMenuItem1.Click += new System.EventHandler(this.consultarToolStripMenuItem1_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(689, -3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 33);
            this.label22.TabIndex = 196;
            this.label22.Text = "_";
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(716, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 25);
            this.label9.TabIndex = 195;
            this.label9.Text = "X";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.White;
            this.pictureBox4.Location = new System.Drawing.Point(-5, 64);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(806, 5);
            this.pictureBox4.TabIndex = 193;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(300, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 55);
            this.label1.TabIndex = 192;
            this.label1.Text = "Menu";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Black;
            this.pictureBox2.Location = new System.Drawing.Point(-21, -5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(822, 74);
            this.pictureBox2.TabIndex = 191;
            this.pictureBox2.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(337, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 18);
            this.label7.TabIndex = 220;
            this.label7.Text = "Serviço de Voz";
            this.label7.Click += new System.EventHandler(this.label7_Click_1);
            // 
            // btnParar
            // 
            this.btnParar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnParar.BackgroundImage")));
            this.btnParar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnParar.Location = new System.Drawing.Point(411, 119);
            this.btnParar.Name = "btnParar";
            this.btnParar.Size = new System.Drawing.Size(34, 34);
            this.btnParar.TabIndex = 219;
            this.btnParar.UseVisualStyleBackColor = true;
            this.btnParar.Click += new System.EventHandler(this.btnParar_Click);
            // 
            // btnFalar
            // 
            this.btnFalar.BackColor = System.Drawing.Color.Transparent;
            this.btnFalar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFalar.BackgroundImage")));
            this.btnFalar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFalar.Location = new System.Drawing.Point(337, 119);
            this.btnFalar.Name = "btnFalar";
            this.btnFalar.Size = new System.Drawing.Size(34, 34);
            this.btnFalar.TabIndex = 218;
            this.btnFalar.UseVisualStyleBackColor = false;
            this.btnFalar.Click += new System.EventHandler(this.btnFalar_Click_1);
            // 
            // lblFalar
            // 
            this.lblFalar.Location = new System.Drawing.Point(310, 93);
            this.lblFalar.Name = "lblFalar";
            this.lblFalar.Size = new System.Drawing.Size(165, 20);
            this.lblFalar.TabIndex = 217;
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Wheat;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(746, 501);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnParar);
            this.Controls.Add(this.btnFalar);
            this.Controls.Add(this.lblFalar);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.ptbFornecedor);
            this.Controls.Add(this.lblus);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ptbProduto);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.ptbFolhaPagamento);
            this.Controls.Add(this.ptbCliente);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.Menu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ptbFornecedor)).EndInit();
            this.CMSFornecedor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptbProduto)).EndInit();
            this.CMSProduto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.CMSFuncionario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.CMSPedidoVenda.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptbFolhaPagamento)).EndInit();
            this.CMSFolhaPagamento.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptbCliente)).EndInit();
            this.CMSCliente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox ptbFornecedor;
        private System.Windows.Forms.Label lblus;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox ptbProduto;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox ptbFolhaPagamento;
        private System.Windows.Forms.PictureBox ptbCliente;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ContextMenuStrip CMSCliente;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip CMSProduto;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem2;
        private System.Windows.Forms.ContextMenuStrip CMSFornecedor;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem3;
        private System.Windows.Forms.ContextMenuStrip CMSFuncionario;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem4;
        private System.Windows.Forms.ContextMenuStrip CMSPedidoVenda;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem5;
        private System.Windows.Forms.ContextMenuStrip CMSFolhaPagamento;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem novaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem novaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estoqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem gastosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarGastoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarGastoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pedidosPendentesToolStripMenuItem;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnParar;
        private System.Windows.Forms.Button btnFalar;
        private System.Windows.Forms.TextBox lblFalar;
    }
}
﻿using Panetteria_Fiorentina.DB.Cliente;
using Panetteria_Fiorentina.DB.Compra;
using Panetteria_Fiorentina.DB.Estoque;
using Panetteria_Fiorentina.DB.Produto;
using Panetteria_Fiorentina.Telas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class ConsultarProduto : Form
    {
        void CarregarGrid()
            {
            string nome = txtNome.Text;
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Consultar();

            dataGridView2.AutoGenerateColumns = false;
            dataGridView2.DataSource = lista;
        }
        public ConsultarProduto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }


        private void ConsultarProduto_Load(object sender, EventArgs e)
        {

        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
      

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
                if (e.ColumnIndex == 1)
                {
                   ProdutoDTO fornecedor = dataGridView2.CurrentRow.DataBoundItem as ProdutoDTO;

                    DialogResult r = MessageBox.Show("Deseja excluir o produto? (Ao excluir o produto será excluido automaticamente todos as produções e compras que conter esse produto.)", "Panetteria Fiorentino",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        
                        ProdutoBusiness a = new ProdutoBusiness();
                        ItensPedidoBusiness b = new ItensPedidoBusiness();
                        
                        EstoqueBusiness d = new EstoqueBusiness();

                        d.Remover(fornecedor.Id);
                         b.RemoverProduto(fornecedor.Id);
                        a.Remover(fornecedor.Id);
                       

                        CarregarGrid();
                    }
                }
            
            
            if (e.ColumnIndex == 0)
            {
                ProdutoDTO a = dataGridView2.CurrentRow.DataBoundItem as ProdutoDTO;

                DialogResult r = MessageBox.Show("Deseja alterar o produto?", "Panetteria Fiorentino",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ProdutoDTO b = this.dataGridView2.Rows[e.RowIndex].DataBoundItem as ProdutoDTO;
                    this.Close();
                    AlterarProduto tela = new AlterarProduto();
                    tela.LoadScrean(b);
                    tela.ShowDialog();
                }
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            MenuPrincipal a = new MenuPrincipal();
            a.Show();
            this.Hide();
        }
    }
    
}

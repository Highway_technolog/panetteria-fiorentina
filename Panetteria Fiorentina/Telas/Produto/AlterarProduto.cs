﻿using Panetteria_Fiorentina.DB.Fornecedor;
using Panetteria_Fiorentina.DB.Produto;
using Panetteria_Fiorentina.Telas.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina.Telas.Produto
{
    public partial class AlterarProduto : Form
    {
        public AlterarProduto()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {


            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> listar = business.Listar();



            cboFor.ValueMember = nameof(FornecedorDTO.Id);
            cboFor.DisplayMember = nameof(FornecedorDTO.Nome);
            cboFor.DataSource = listar;
        }

        private void AlterarProduto_Load(object sender, EventArgs e)
        {

        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        ProdutoDTO produto2;
        public void LoadScrean(ProdutoDTO produto)
        {
            this.produto2 = produto;

            txtnome.Text = produto.Nome;
            comboBox1.SelectedItem = produto.Tipo;
            txtval.Text = produto.ValorUnitario.ToString();
            cboFor.SelectedItem = produto.FornecedorNome;
            



        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                FornecedorDTO forne = cboFor.SelectedItem as FornecedorDTO;


               

                this.produto2.Nome = txtnome.Text;
                this.produto2.FornecedorNome = forne.Nome;
                this.produto2.IdFornecedor = forne.Id;
                this.produto2.Tipo = comboBox1.SelectedItem.ToString();
                this.produto2.ValorUnitario = Convert.ToDecimal(txtval.Text);
            

                ProdutoBusiness business = new ProdutoBusiness();
                business.Alterar(produto2);

                EnviarMensagem("Produto alterado com sucesso");

                ConsultarProduto a = new ConsultarProduto();
                a.Show();
                
                
                

            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possível alterar o produto: " + ex.Message);
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            ConsultarProduto a = new ConsultarProduto();
            a.Show();
            this.Hide();
        }
    }

}


﻿using Panetteria_Fiorentina.DB.Cliente;
using Panetteria_Fiorentina.DB.Fornecedor;
using Panetteria_Fiorentina.DB.Produto;
using Panetteria_Fiorentina.Telas.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class CadastrarProduto : Form
    {
        public CadastrarProduto()
        {
            InitializeComponent();
            CarregarCombos();

        }

        void CarregarCombos()
        {


            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> listar = business.Listar();

            

            cboFor.ValueMember = nameof(FornecedorDTO.Id);
            cboFor.DisplayMember = nameof(FornecedorDTO.Nome);
            cboFor.DataSource = listar;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                FornecedorDTO forne = cboFor.SelectedItem as FornecedorDTO;
                

                ProdutoDTO produto = new ProdutoDTO();
                produto.Nome = txtnome.Text;
                produto.FornecedorNome = forne.Nome;
                produto.IdFornecedor = forne.Id;
                produto.Tipo = comboBox1.Text;
                produto.ValorUnitario = Convert.ToDecimal(txtvalor.Text);
     

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(produto);
                
                EnviarMensagem("Produto salvo com sucesso");
                EnviarMensagemNovo("Deseja cadastrar um novo produto ?");

          }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possível cadastrar o produto: " + ex.Message);
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
           
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void CadastrarProduto_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        public void Limpar()
        {
            txtnome.Text = "";
            comboBox1.SelectedItem = "Selecione";
            txtvalor.Text = "";
            cboFor.SelectedText = "";
           
        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }


        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void cboFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedorDTO func = cboFor.SelectedItem as FornecedorDTO;

            
          
        }
    }
}

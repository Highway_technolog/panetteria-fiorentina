﻿using Panetteria_Fiorentina.DB.Estoque;
using Panetteria_Fiorentina.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class Estoque : Form
    {
        public Estoque()
        {
            InitializeComponent();
            ConfigurarGrid();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }
        
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }


        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        
        public void ConfigurarGrid()
        {
            EstoqueBusiness Business = new EstoqueBusiness();
            List<EstoqueDTO> lista = Business.Consultar();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text == "")
                {
                    ConfigurarGrid();
                }
                else
                {
                    string nome = textBox1.Text;
                    EstoqueBusiness Business = new EstoqueBusiness();
                    List<EstoqueDTO> lista = Business.ConsultarPorNome(nome);

                    dataGridView1.AutoGenerateColumns = false;
                    dataGridView1.DataSource = lista;
                }
            }
            catch(Exception ex)
            {
                EnviarMensagemErro("Não foi possivel realizar a consulta: " + ex.Message);
            }
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                DialogResult r = MessageBox.Show("Deseja comprar mais ?", "Panetteria Fiorentino",
                                     MessageBoxButtons.YesNo,
                                     MessageBoxIcon.Question);
                if (r == DialogResult.Yes)
                {
                    EstoqueDTO b = this.dataGridView1.Rows[e.RowIndex].DataBoundItem as EstoqueDTO;
                    this.Hide();
                    PedidoCompra a = new PedidoCompra();
                    a.LoadScrean(b);
                    a.ShowDialog();
                }
            }
            if(e.ColumnIndex == 5)
            {
                DialogResult r = MessageBox.Show("Deseja retirar o produto?", "Panetteria Fiorentino",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    EstoqueDTO dto = dataGridView1.Rows[e.RowIndex].DataBoundItem as EstoqueDTO;
                    if (dto.Quantidade == 0)
                    {
                        MessageBox.Show("Esse produto esgotou.", "Panetteria Fiorentino",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        EstoqueBusiness bus = new EstoqueBusiness();
                        bus.RemoverItem(dto);
                        ConfigurarGrid();
                    }
                }
                
            }
        }

        private void Estoque_Load(object sender, EventArgs e)
        {

        }
    }
}

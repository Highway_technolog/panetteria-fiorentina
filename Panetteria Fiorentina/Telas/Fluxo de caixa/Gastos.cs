﻿using Panetteria_Fiorentina.DB.Fluxo_de_Caixa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina.Telas.Fluxo_de_caixa
{
    public partial class Gastos : Form
    {
        public Gastos()
        {
            InitializeComponent();
            CarregarCombo();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal a = new MenuPrincipal();
            a.Show();
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        public void Limpar()
        {
            txtnome.Text = "";
            dtp1.Value = DateTime.Now.Date;
            cbotipo.SelectedItem = "Selecione";
            txtvalor.Text = "";
        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        public void CarregarCombo()
        {
            List<String> a = new List<string>();
            a.Add("Selecione");
            a.Add("Variavel");
            a.Add("Fixo");
            cbotipo.DataSource = a;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                GastosDTO dto = new GastosDTO();

                dto.Nome = txtnome.Text;
                dto.Data = dtp1.Value.Date;
                dto.Tipo = cbotipo.SelectedItem.ToString();
                dto.Valor = Convert.ToDecimal(txtvalor.Text);

                GastosBusiness bus = new GastosBusiness();
                bus.Salvar(dto);

                EnviarMensagem("Gasto salvo com sucesso.");
                EnviarMensagemNovo("Deseja cadastrar outro gasto ?");
            }
            catch (Exception ex)
            {
                EnviarMensagemErro(ex.Message);
            }
        }
       
        private void button2_Click(object sender, EventArgs e)
        {
            Limpar();
        }
    }
}

﻿using Newtonsoft.Json;
using Panetteria_Fiorentina.DB.Funcionario;
using Panetteria_Fiorentina.Telas.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class CadastroFuncionario : Form
    {
        public CadastroFuncionario()
        {
            InitializeComponent();


            Cargos.Add(cargo0);
            Cargos.Add(cargo1);
            Cargos.Add(cargo2);
            Cargos.Add(cargo3);
            Cargos.Add(cargo4);
            Cargos.Add(cargo5);
            Cargos.Add(cargo6);
            Cargos.Add(cargo7);
            Cargos.Add(cargo8);
            Cargos.Add(cargo9);
            Cargos.Add(cargo10);
            Cargos.Add(cargo11);
            Cargos.Add(cargo12);
            Cargos.Add(cargo13);

            CBCArgo.DataSource = Cargos;
            CBCArgo.SelectedText = "Selecione o Cargo";



        }
        List<String> Cargos = new List<string>();
        string cargo0 = "Selecione o Cargo";
        string cargo1 = "Atendende de driver trhu";
        string cargo2 = "Atendente";
        string cargo3 = "Balconista";
        string cargo4 = "Copeiro";
        string cargo5 = "Entregador";
        string cargo6 = "Entragadores delivery";
        string cargo7 = "Auxiliar de caixa";
        string cargo8 = "Operador de Caixa";
        string cargo9 = "Padeiro";
        string cargo10 = "Pizzaiolo";
        string cargo11 = "Repositor";
        string cargo12 = "Administrador";
        string cargo13 = "Gerente";







        private void button5_Click(object sender, EventArgs e)
        {
            try
            {


                FuncionarioDTO funcionario = new FuncionarioDTO();
                funcionario.Nome = NomeText.Text;
                funcionario.CPF = CPFtext.Text;
                funcionario.Endereço = EndercoText.Text;
                funcionario.Numero = Numerotext.Text;
                funcionario.CEP = CEPtext.Text;
                funcionario.Celular = Celtext.Text;
                funcionario.Nascimento = DTPNascimento.Value.Date;
                funcionario.Salario = Convert.ToDecimal(salarioText.Text);
                funcionario.Email = Emailtext.Text;
                funcionario.Complemento = ComplementoText.Text;
                /*if(imgFuncionario.BackgroundImage=! null)
                {
                    funcionario.Foto = ImagemPlugin.ConverterParaString(imgFuncionario.Image);
                }*/
                
                funcionario.RG = RGtext.Text;
                funcionario.Telefone = Teltext.Text;
                funcionario.Cargo = CBCArgo.Text;


                funcionario.Usuario = Usuariotext.Text;
                funcionario.Senha = senhaText.Text;
                funcionario.Admin = chkAdmin.Checked;
                funcionario.RH = chkRHH.Checked;
                funcionario.Logistica = chkLogistica.Checked;
                funcionario.Financeiro = chkFinanceiro.Checked;
                funcionario.Vendas = chkVendas.Checked;
                funcionario.Compras = chkCompras.Checked;
                funcionario.Empregado = chkEmpregado.Checked;




                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(funcionario);

                EnviarMensagem("Funcionário salvo com sucesso.");
                EnviarMensagemNovo("Deseja cadastrar outro funcionário ?");






            }
            
            
            catch (Exception ex)
            {
                EnviarMensagemErro("Ocorreu um erro ao salvar o funcionário: " + ex.Message);
            }
        }









        public void Limpar()
        {
            NomeText.Text = "";
            CPFtext.Text = "";
            EndercoText.Text = "";
            Numerotext.Text = "";
            CEPtext.Text = "";
            Celtext.Text = "";

            salarioText.Text = "";
            Emailtext.Text = "";
            ComplementoText.Text = "";
            RGtext.Text = "";
            Teltext.Text = "";
            CBCArgo.Text = "";


            Usuariotext.Text = "";
            senhaText.Text = "";
            chkAdmin.Checked = false;
            chkRHH.Checked = false;
            chkLogistica.Checked = false;
            chkFinanceiro.Checked = false;
            chkVendas.Checked = false;
            chkCompras.Checked = false;
            chkEmpregado.Checked = false;
        }


       


        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }

        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }


        private void imgProduto_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgFuncionario.ImageLocation = dialog.FileName;
            }
        }



        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label12_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }



        private void imgFuncionario_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgFuncionario.ImageLocation = dialog.FileName;
            }
        }

     private void btnBuscarCEP_Click(object sender, EventArgs e)
                {

                    string cep = CEPtext.Text.Trim().Replace("-", "");

                    CorreioResponseFunc correio = BuscarAPICorreio(cep);

                    EndercoText.Text = correio.Logradouro;
                }
                private CorreioResponseFunc BuscarAPICorreio(string cep)
                {

                    WebClient rest = new WebClient();
                    rest.Encoding = Encoding.UTF8;


                    string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");


                  CorreioResponseFunc correio = JsonConvert.DeserializeObject<CorreioResponseFunc>(resposta);
                    return correio;
                }

        private void chkAdmin_CheckedChanged(object sender, EventArgs e)
        {
             if (UserSession.UsuarioLogado.Admin == false)
                {
                chkAdmin.Checked = false;
                }
            
        }

        private void chkAdmin_EnabledChanged(object sender, EventArgs e)
        {

        }
    }
}



    
    

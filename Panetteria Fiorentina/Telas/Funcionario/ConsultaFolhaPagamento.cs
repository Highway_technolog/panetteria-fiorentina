﻿using Panetteria_Fiorentina.DB.Funcionario;
using Panetteria_Fiorentina.DB.RH;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class ConsultaFolhaPagamento : Form
    {
        public ConsultaFolhaPagamento()
        {
            InitializeComponent();
        }
       
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        void CarregarGrid()
        {

            string nome = txtNome.Text;
            FolhaPagamentoBusiness Business = new FolhaPagamentoBusiness();
            List<FolhaPagamentoDTO> lista = Business.Consultar();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;

        }

        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            try
            {
                CarregarGrid();
            }
            catch(Exception ex)
            {
                EnviarMensagemErro("Não foi possivel realizar a consulta: " + ex.Message);
            }
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Close();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            

            if (e.ColumnIndex == 0)
            {
                FolhaPagamentoDTO funcionario = dataGridView1.CurrentRow.DataBoundItem as FolhaPagamentoDTO;

                DialogResult r = MessageBox.Show("Deseja excluir a folha de pagamento?", "Panetteria Fiorentino",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FolhaPagamentoBusiness business = new FolhaPagamentoBusiness();
                    business.Remover(funcionario.Id);

                    CarregarGrid();
                }
            }
            
            
        }

        private void ConsultaFolhaPagamento_Load(object sender, EventArgs e)
        {

        }
    }
}

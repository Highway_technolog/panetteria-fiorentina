﻿using Panetteria_Fiorentina.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class ConsultarFuncionario : Form
    {
        public ConsultarFuncionario()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void ConsultarFuncionario_Load(object sender, EventArgs e)
        {

        }

        public void CarregarGrid()
        {
            string nome = txtNome.Text;
            FuncionarioBusiness Business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = Business.Consultar(nome);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;







        }
        private void button1_Click(object sender, EventArgs e)
        {



            try
             {
                 CarregarGrid();
             }
             catch (Exception ex)
             {
                 MessageBox.Show(ex.Message);
             }
        }



        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Close();

            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }
        
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label9_Click(object sender, EventArgs e)
        {
            
        }

        private void label9_Click_1(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        public void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                FuncionarioDTO funcionario = dataGridView1.CurrentRow.DataBoundItem as FuncionarioDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o funcionário?", "Panetteria Fiorentino",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(funcionario.ID);

                    CarregarGrid();
                }
            }
            if (e.ColumnIndex == 0)
            {
                FuncionarioDTO funcionario = dataGridView1.CurrentRow.DataBoundItem as FuncionarioDTO;

                DialogResult r = MessageBox.Show("Deseja alterar o funcionário?", "Panetteria Fiorentino",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FuncionarioDTO dto = dataGridView1.Rows[e.RowIndex].DataBoundItem as FuncionarioDTO;
                    this.Close();
                    AlterarFuncionario tela = new AlterarFuncionario();
                    tela.LoadScrean(dto);
                    tela.ShowDialog();
                }
            }

        }

        private void label22_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}

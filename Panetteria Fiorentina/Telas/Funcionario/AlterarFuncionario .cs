﻿using Newtonsoft.Json;
using Panetteria_Fiorentina.DB.Funcionario;
using Panetteria_Fiorentina.Telas.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class AlterarFuncionario : Form
    {
        public AlterarFuncionario()
        {
            InitializeComponent();


            Cargos.Add(cargo0);
            Cargos.Add(cargo1);
            Cargos.Add(cargo2);
            Cargos.Add(cargo3);
            Cargos.Add(cargo4);
            Cargos.Add(cargo5);
            Cargos.Add(cargo6);
            Cargos.Add(cargo7);
            Cargos.Add(cargo8);
            Cargos.Add(cargo9);
            Cargos.Add(cargo10);
            Cargos.Add(cargo11);
            Cargos.Add(cargo12);
            Cargos.Add(cargo13);

            CBCArgo.DataSource = Cargos;
            CBCArgo.SelectedText = "Selecione o Cargo";



        }
        List<String> Cargos = new List<string>();
        string cargo0 = "Selecione o Cargo";
        string cargo1 = "Atendende de driver trhu";
        string cargo2 = "Atendente";
        string cargo3 = "Balconista";
        string cargo4 = "Copeiro";
        string cargo5 = "Entregador";
        string cargo6 = "Entragadores delivery";
        string cargo7 = "Auxiliar de caixa";
        string cargo8 = "Operador de Caixa";
        string cargo9 = "Padeiro";
        string cargo10 = "Pizzaiolo";
        string cargo11 = "Repositor";
        string cargo12 = "Administrador";
        string cargo13 = "Gerente";



        FuncionarioDTO funcionario;


       public void LoadScrean(FuncionarioDTO funcionario)
        {
            this.funcionario = funcionario;

            
            NomeText.Text = funcionario.Nome;
            CPFtext.Text = funcionario.CPF;
            EndercoText.Text = funcionario.Endereço;
            Numerotext.Text = funcionario.Numero;
            CEPtext.Text = funcionario.CEP;
            Celtext.Text = funcionario.Celular;
            DTPNascimento.Value = funcionario.Nascimento;
            salarioText.Text = Convert.ToString(funcionario.Salario);
            Emailtext.Text = funcionario.Email;
            ComplementoText.Text = funcionario.Complemento;
            // funcionario.Foto = ImagemPlugin.ConverterParaString(imgFuncionario.Image);
            RGtext.Text = funcionario.RG;
            Teltext.Text = funcionario.Telefone;
            CBCArgo.Text = funcionario.Cargo;


            Usuariotext.Text = funcionario.Usuario;
            senhaText.Text = funcionario.Senha;
            chkAdmin.Checked = funcionario.Admin;
            chkRHH.Checked = funcionario.RH;
            chkLogistica.Checked = funcionario.Logistica;
            chkFinanceiro.Checked = funcionario.Financeiro;
            chkVendas.Checked = funcionario.Vendas;
            chkCompras.Checked = funcionario.Compras;
            chkEmpregado.Checked = funcionario.Empregado;

        }



        private void button5_Click(object sender, EventArgs e)
        {
            try { 
                    
                this.funcionario.Nome = NomeText.Text;
                this.funcionario.CPF = CPFtext.Text;
                this.funcionario.Endereço = EndercoText.Text;
                this.funcionario.Numero = Numerotext.Text;
                this.funcionario.CEP = CEPtext.Text;
                this.funcionario.Celular = Celtext.Text;
                this.funcionario.Nascimento = DTPNascimento.Value.Date;
                this.funcionario.Salario = Convert.ToDecimal(salarioText.Text);
                this.funcionario.Email = Emailtext.Text;
                this.funcionario.Complemento = ComplementoText.Text;
                //this.funcionario.Foto = ImagemPlugin.ConverterParaString(imgFuncionario.Image);
                this.funcionario.RG = RGtext.Text;
                this.funcionario.Telefone = Teltext.Text;
                this.funcionario.Cargo = CBCArgo.Text;


                this.funcionario.Usuario = Usuariotext.Text;
                this.funcionario.Senha = senhaText.Text;
                this.funcionario.Admin = chkAdmin.Checked;
                this.funcionario.RH = chkRHH.Checked;
                this.funcionario.Logistica = chkLogistica.Checked;
                this.funcionario.Financeiro = chkFinanceiro.Checked;
                this.funcionario.Vendas = chkVendas.Checked;
                this.funcionario.Compras = chkCompras.Checked;
                this.funcionario.Empregado = chkEmpregado.Checked;
               



                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Alterar(funcionario);

                EnviarMensagem("Funcionário alterado com sucesso.");


                this.Close();

                ConsultarFuncionario cadastro = new ConsultarFuncionario();

                cadastro.Show();


            }
           
            catch (Exception ex)
            {
                EnviarMensagemErro("Ocorreu um erro ao alterar o funcionário: " + ex.Message);
            }
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }

        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }


        private void imgProduto_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgFuncionario.ImageLocation = dialog.FileName;
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkRHH_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void CadastroFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
            ConsultarFuncionario tela = new ConsultarFuncionario();
            tela.Show();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label12_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void button4_Click(object sender, EventArgs e)
        {
           
        
        }

        private void TextCPF(object sender, EventArgs e)
        {

        }

        private void imgFuncionario_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void btnBuscarCEP_Click(object sender, EventArgs e)
        {

        }
     
    }
}

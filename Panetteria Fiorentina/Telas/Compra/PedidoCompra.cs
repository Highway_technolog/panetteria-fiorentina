﻿using Panetteria_Fiorentina.DB.Compra;
using Panetteria_Fiorentina.DB.Estoque;
using Panetteria_Fiorentina.DB.Fornecedor;
using Panetteria_Fiorentina.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class PedidoCompra : Form
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        BindingList<FornecedorDTO> fornecedor = new BindingList<FornecedorDTO>();

        public PedidoCompra()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }
       

        public void LoadScrean(EstoqueDTO pedido)
        {
           
            cbofor.SelectedItem = pedido.Fornecedor;
            cboproduto.SelectedItem = pedido.Produto;

        }
        void ConfigurarGrid()
        {
            dgvCarrinho.AutoGenerateColumns = false;
            dgvCarrinho.DataSource = produtosCarrinho;
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
               Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }
       
        void CarregarCombos()
        {


            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> listar = business.Listar();

            cbofor.SelectedItem = 0;
            cbofor.ValueMember = nameof(FornecedorDTO.Id);
            cbofor.DisplayMember = nameof(FornecedorDTO.Nome);
            cbofor.DataSource = listar;
           
            




        }

        private void button2_Click(object sender, EventArgs e)
        {

            try
            {
                FornecedorDTO fornecedor = cbofor.SelectedItem as FornecedorDTO;


                PedidoDTO pedido = new PedidoDTO();

                pedido.NomeFornecedor = fornecedor.Nome;
                pedido.Data = DateTime.Now.Date;
                pedido.Valor = Convert.ToDecimal(lbltotal.Text);
                pedido.IdFornecedor = fornecedor.Id;


                PedidoBusiness t = new PedidoBusiness();
                t.Salvar(pedido, produtosCarrinho.ToList());


                EstoqueBusiness bus = new EstoqueBusiness();




                EnviarMensagem("Compra finalizada com sucesso.");
                EnviarMensagemNovo("Deseja efetuar outra compra ?");

            }
            catch(Exception ex)
            {
                EnviarMensagemErro("Houve um erro ao efutuar a compra: " + ex.Message);
            }

                

            
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                ProdutoDTO dto = cboproduto.SelectedItem as ProdutoDTO;
                if (txtquan.Text == "")
                {
                    throw new Exception("Informe a quantidade do produto.");
                }

                decimal qtd = Convert.ToInt32(txtquan.Text);

                for (int i = 0; i < qtd; i++)
                {
                    int qtd2 = 1;
                    produtosCarrinho.Add(dto);
                    decimal total = Convert.ToDecimal(lbltotal.Text);
                    decimal valor = Convert.ToDecimal(dto.ValorUnitario * qtd2);
                    decimal valor2 = valor + total;
                    lbltotal.Text = valor2.ToString();
                }
            }
            catch (Exception ex)
            {
                EnviarMensagemErro(ex.Message);
            }
                

               
                
                
           
        }

        private void label12_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
            this.Close();
        }

        private void cboproduto_SelectedIndexChanged(object sender, EventArgs e)
        {

            

        }

        private void cbofor_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedorDTO func = cbofor.SelectedItem as FornecedorDTO;
            int id = func.Id;

            ProdutoBusiness business2 = new ProdutoBusiness();
            List<ProdutoDTO> listar2 = business2.ListarPorFornecedor(id);
            cboproduto.ValueMember = nameof(ProdutoDTO.Id);
            cboproduto.DisplayMember = nameof(ProdutoDTO.Nome);



            cboproduto.DataSource = listar2;



        }


        private void PedidoCompra_Load(object sender, EventArgs e)
        {

        }


        public void Limpar()
        {
            for (int i = 0; i < dgvCarrinho.RowCount; i++)
            {
                dgvCarrinho.Rows[i].DataGridView.Rows.Clear();
            }
            txtquan.Text = "";
        }
        private void button4_Click(object sender, EventArgs e)
        {

            Limpar();


        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void dgvCarrinho_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                dgvCarrinho.Rows.RemoveAt(dgvCarrinho.CurrentRow.Index);

                ProdutoDTO dto = cboproduto.SelectedItem as ProdutoDTO;
                decimal total = Convert.ToDecimal(lbltotal.Text);
                decimal valor = Convert.ToDecimal((total - dto.ValorUnitario));
                lbltotal.Text = valor.ToString();
            }
        }
    }
}

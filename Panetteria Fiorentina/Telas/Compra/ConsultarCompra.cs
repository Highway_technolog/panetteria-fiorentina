﻿using Panetteria_Fiorentina.DB.Compra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina.Telas.Compra
{
    public partial class ConsultarCompra : Form
    {
        public ConsultarCompra()
        {
            InitializeComponent();
            dateTimePicker1.Value = DateTime.Now.Date; 
        }
        void ConfigurarGrid()
        {
            DateTime a = DateTime.Now;
          
            PedidoBusiness Business = new PedidoBusiness();
            List<PedidoDTO> lista = Business.Consultar(dtp1.Value, dateTimePicker1.Value);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ConfigurarGrid();
            }
            catch(Exception ex)
            {
                EnviarMensagemErro("Aconteceu um erro ao consultar a compra: "+ ex.Message);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
            this.Close();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void ConsultarCompra_Load(object sender, EventArgs e)
        {

        }
    }
}

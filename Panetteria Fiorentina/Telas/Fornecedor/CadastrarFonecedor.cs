﻿using Newtonsoft.Json;
using Panetteria_Fiorentina.DB.Fornecedor;
using Panetteria_Fiorentina.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class CadastrarFonecedor : Form
    {
        public CadastrarFonecedor()
        {
            InitializeComponent();
            CarregarCombo();
        }
        public void CarregarCombo()
        {
            List<string> estado = new List<string>();
            estado.Add("Selecione um estado");
            estado.Add("Acre(AC)");
            estado.Add("Alagoas (AL)");
            estado.Add("Amapá (AP)");
            estado.Add("Amazonas (AM)");
            estado.Add("Bahia (BA)");
            estado.Add("Ceará (CE)");
            estado.Add("Distrito Federal (DF)");
            estado.Add("Espírito Santo (ES)");
            estado.Add("Goiás (GO)");
            estado.Add("Maranhão (MA)");
            estado.Add("Mato Grosso (MT)");
            estado.Add("Mato Grosso do Sul (MS)");
            estado.Add("Minas Gerais (MG)");
            estado.Add("Pará (PA) ");
            estado.Add("Paraíba (PB)");
            estado.Add("Paraná (PR)");
            estado.Add("Pernambuco (PE)");
            estado.Add("Piauí (PI)");
            estado.Add("Rio de Janeiro (RJ)");
            estado.Add("Rio Grande do Norte (RN)");
            estado.Add("Rio Grande do Sul (RS)");
            estado.Add("Rondônia (RO)");
            estado.Add("Roraima (RR)");
            estado.Add("Santa Catarina (SC)");
            estado.Add("São Paulo (SP)");
            estado.Add("Sergipe (SE)");
            estado.Add("Tocantins (TO)");

            cboCidade.DataSource = estado;
            cboCidade.SelectedItem = "Selecione um estado";






        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
            this.Close();
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }
        public void Limpar()
        {
            txtNome.Text = "";
            txtCNPJ.Text = "";
            txtContato.Text = "";
            txtEmail.Text = "";
            txtCEP.Text = "";
            txtEndereco.Text = "";
            txtNumero.Text = "";
            cboCidade.SelectedItem = "Selecione um estado";
            txtObs.Text = "";


        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO fornecedor = new FornecedorDTO();

                fornecedor.Nome = txtNome.Text;
                fornecedor.CNPJ = txtCNPJ.Text;
                fornecedor.Telefone = txtContato.Text;
                fornecedor.Email = txtEmail.Text;
                fornecedor.CEP = txtCEP.Text;
                fornecedor.Endereco = txtEndereco.Text;
                fornecedor.Cidade = cboCidade.SelectedItem.ToString();
                fornecedor.Numero = Convert.ToString(txtNumero.Text);
                fornecedor.Observacao = txtObs.Text;
                fornecedor.Cadastro = DateTime.Now.Date;

                FornecedorBusiness business = new FornecedorBusiness();
                business.Salvar(fornecedor);




                EnviarMensagem("Fornecedor salvo com sucesso");
                EnviarMensagemNovo("Deseja cadastrar outro fornecedor ?");


            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possível cadastrar o fornecerdor: " + ex.Message);
            }
        }

        private void maskedTextBox9_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private void txtEmail_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void txtNumero_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {

        }

        private void cboCidade_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void txtEndereco_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void txtObs_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCEP_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtCNPJ_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtContato_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtNome_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void CadastrarFonecedor_Load(object sender, EventArgs e)
        {

        }

        private void btnBuscarCEP_Click(object sender, EventArgs e)
        {
            



                string cep = txtCEP.Text.Trim().Replace("-", "");

                CorreioFornecedor correio = BuscarAPICorreio(cep);

                txtEndereco.Text = correio.Logradouro;
            
        }
        private CorreioFornecedor BuscarAPICorreio(string cep)
        {

            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;


            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");


            CorreioFornecedor correio = JsonConvert.DeserializeObject<CorreioFornecedor>(resposta);
            return correio;
        }
    }
  }

       
           
    

    


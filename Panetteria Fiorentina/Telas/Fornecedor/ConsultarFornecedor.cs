﻿using Panetteria_Fiorentina.DB.Compra;
using Panetteria_Fiorentina.DB.Estoque;
using Panetteria_Fiorentina.DB.Fornecedor;
using Panetteria_Fiorentina.DB.Produto;
using Panetteria_Fiorentina.Telas.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class ConsultarFornecedor : Form
    {
        public ConsultarFornecedor()
        {
            InitializeComponent();
        }

        private void ConsultarFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }
       

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Information);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }


        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }
        public void CarregarGrid()
        {
            string nome = txtNome.Text;
            FornecedorBusiness Business = new FornecedorBusiness();
            List<FornecedorDTO> lista = Business.Consultar();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }
        public void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
                /*if (e.ColumnIndex == 1)
                {
                    FornecedorDTO fornecedor = dataGridView1.CurrentRow.DataBoundItem as FornecedorDTO;

                    DialogResult r = MessageBox.Show("Deseja excluir o fornecedor? (Ao excluir o fornecedor, todos seus produtos e pedidos serão excluidos automaticamente.)", "Panetteria Fiorentino",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        FornecedorBusiness business = new FornecedorBusiness();
                        ProdutoBusiness a = new ProdutoBusiness();
                        ItensPedidoBusiness b = new ItensPedidoBusiness();

                        
                      
                        EstoqueBusiness d = new EstoqueBusiness();

                        d.RemoverFornecedor(fornecedor.Id);
                        b.Remover(fornecedor.Id);
                        
                        a.RemoverFornecedor(fornecedor.Id);
                        business.Remover(fornecedor.Id);

                        CarregarGrid();
                    }
                }*/
            
           
            if (e.ColumnIndex == 0)
            {
                FornecedorDTO a = dataGridView1.CurrentRow.DataBoundItem as FornecedorDTO;

                DialogResult r = MessageBox.Show("Deseja alterar o fornecedor?", "Panetteria Fiorentino",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FornecedorDTO b = this.dataGridView1.Rows[e.RowIndex].DataBoundItem as FornecedorDTO;
                    this.Close();
                    AlterarFornecedor tela = new AlterarFornecedor();
                    tela.LoadScrean(b);
                    tela.ShowDialog();
                }
            }
        }
    }
}

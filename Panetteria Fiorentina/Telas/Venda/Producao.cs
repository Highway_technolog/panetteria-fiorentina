﻿using Panetteria_Fiorentina.DB.Cliente;
using Panetteria_Fiorentina.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina.Telas.Venda
{
    public partial class Producao : Form
    {
        public Producao()
        {
            InitializeComponent();
          
            CarregarCombo();

            
        }
        public void CarregarCombo()
        {
            List<string> tipo = new List<string>();
            tipo.Add("Selecione");
            tipo.Add("Revenda");
            tipo.Add("Produção");

            comboBox1.DataSource = tipo;
            comboBox1.SelectedText = "Selecione";
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                txtnome.Text = "";
                txtvalor.Text = "";
                txtdes.Text = "";
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProduçãoDTO dto = new ProduçãoDTO();
                dto.Nome = txtnome.Text;
                dto.Valor = Convert.ToDecimal(txtvalor.Text);
                dto.Descricao = txtdes.Text;
                dto.Tipo = comboBox1.SelectedItem.ToString();


                ProduçãoBusiness business = new ProduçãoBusiness();
                business.Salvar(dto);


                EnviarMensagem("Item do cardapio salvo com sucesso");
                EnviarMensagemNovo("Deseja cadastrar outro item ?");

            }
            catch (Exception ex)
            {

                EnviarMensagemErro("Não foi possívels salvar item no cardapio " + ex);
            }
                

                
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtnome.Text = "";
            txtvalor.Text = "";
            txtdes.Text = "";
        }

        private void label12_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
    }
}

﻿using Panetteria_Fiorentina.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class ConsultarVenda : Form
    {
        void CarregarGrid()
        {
            DateTime data = dtp1.Value;
            EncomendaBusiness business = new EncomendaBusiness();
            List<EncomendaDTO> lista = business.Consultar(dtp1.Value, dateTimePicker1.Value);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }
        public ConsultarVenda()
        {
            InitializeComponent();
            dateTimePicker1.Value = DateTime.Now.Date;
        }

        private void label9_Click(object sender, EventArgs e)
        {
            
            
                EnviarMensagemFechar();
            }
            void EnviarMensagemFechar()
            {
                DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                          MessageBoxButtons.YesNo,
                          MessageBoxIcon.Warning);

                if (r == DialogResult.Yes)
                {
                    Application.Exit();
                }

            }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void ConsultarVenda_Load(object sender, EventArgs e)
        {

        }
    }

    }


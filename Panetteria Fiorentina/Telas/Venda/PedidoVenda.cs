﻿using Newtonsoft.Json;
using Panetteria_Fiorentina.DB.Cliente;
using Panetteria_Fiorentina.DB.Funcionario;
using Panetteria_Fiorentina.DB.PLUGIN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Panetteria_Fiorentina
{
    public partial class PedidoVenda : Form
    {
        BindingList<ProduçãoDTO> produtosCarrinho = new BindingList<ProduçãoDTO>();
        BindingList<ClienteDTO> cliente = new BindingList<ClienteDTO>();
        public PedidoVenda()
        {
            InitializeComponent();
            cbo1.Visible = false;
            CarregarCombos();
            ConfigurarGrid();
            cbof.SelectedText = "Selecione";
            cbo1.Visible = false;
            lblemail.Visible = false;
            txEmail.Visible = false;
            Teltext.Visible = false;
            txtCep.Visible = false;
            txtEndereco.Visible = false;
            txtnum.Visible = false;
            txtcomplemento.Visible = false;
            lblc.Visible = false;
            lblce.Visible = false;
            lblco.Visible = false;
            lble.Visible = false;
            lblf.Visible = false;
            lbln.Visible = false;
            lblt.Visible = false;
            cbof.Visible = false;
            cbo1.Visible = false;
            txtnome.Visible = false;
            btnBuscarCEP.Visible = false;
            label2.Visible = false;
            comboBox1.Visible = false;

            cbof.SelectedItem = "Selecione";
            comboBox1.SelectedItem = "Selecione";


        }
        void CarregarCombos()
        {


            

            ProduçãoBusiness business2 = new ProduçãoBusiness();
            List<ProduçãoDTO> listar2 = business2.Listar();
            cbo2.ValueMember = nameof(ProduçãoDTO.Id);
            cbo2.DisplayMember = nameof(ProduçãoDTO.Nome);



            cbo2.DataSource = listar2;




        }
        void ConfigurarGrid()
        {
            dgvCarrinho.AutoGenerateColumns = false;
            dgvCarrinho.DataSource = produtosCarrinho;
        }


        private void label12_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }
        private void EnviarMensagemNovo(string mensagem)
        {
            DialogResult r = MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                Limpar();
            }
            else
            {
                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }

        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void PedidoVenda_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProduçãoDTO dto = cbo2.SelectedItem as ProduçãoDTO;
                if (txtquan.Text == "")
                {
                    throw new Exception("Informe a quantidade do produto");
                }

                decimal qtd = Convert.ToInt32(txtquan.Text);

                for (int i = 0; i < qtd; i++)
                {
                    int qtd2 = 1;
                    produtosCarrinho.Add(dto);
                    decimal total = Convert.ToDecimal(lbltotal.Text);
                    decimal valor = Convert.ToDecimal(dto.Valor * qtd2);
                    decimal valor2 = valor + total;
                    lbltotal.Text = valor2.ToString();
                }

                txtquan.Text = "";
            }
            catch (Exception ex)
            {
                EnviarMensagemErro(ex.Message);
            }


        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButton2.Checked == true)
            {
                txtnome.Visible = false;
                btnBuscarCEP.Visible = false;
                cbo1.Visible = true;
                
                lblemail.Visible = true;
                txEmail.Visible = true;
                Teltext.Visible = true;
                txtCep.Visible = true;
                txtEndereco.Visible = true;
                txtnum.Visible = true;
                txtcomplemento.Visible = true;
                lblc.Visible = true;
                lblce.Visible = true;
                lblco.Visible = true;
                lble.Visible = true;
                lblf.Visible = true;
                lbln.Visible = true;
                lblt.Visible = true;
                cbof.Visible = true;
                label2.Visible = true;
                comboBox1.Visible = true;


                ClienteBusiness business = new ClienteBusiness();
                List<ClienteDTO> listar = business.Listar();

                Teltext.Text = "";
                txtCep.Text = "";
                txtEndereco.Text = "";
                txtcomplemento.Text = "";
                txtnum.Text = "";
                txEmail.Text = "";
                txtnome.Text = "";
                cbo1.SelectedItem = 0;
                cbof.SelectedItem = 0;
                comboBox1.SelectedItem = 0;
                cbo1.ValueMember = nameof(ClienteDTO.ID);
                cbo1.DisplayMember = nameof(ClienteDTO.Nome);
                cbo1.DataSource = listar;
               



            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                cbo1.Visible = false;
                lblemail.Visible = false;
                txEmail.Visible = false;
                btnBuscarCEP.Visible = true;
                Teltext.Visible = true;
                txtCep.Visible = true;
                txtEndereco.Visible = true;
                txtnum.Visible = true;
                txtcomplemento.Visible = true;
                lblc.Visible = true;
                lblce.Visible = true;
                lblco.Visible = true;
                lble.Visible = true;
                lblf.Visible = true;
                lbln.Visible = true;
                lblt.Visible = true;
                cbof.Visible = true;
                txtnome.Visible = true;
                label2.Visible = true;
                comboBox1.Visible = true;


                txtCep.Text = "";
                Teltext.Text = "";
                txtEndereco.Text = "";
                txtcomplemento.Text = "";
                txtnum.Text = "";
                txEmail.Text = "";
                txtnome.Text= "";
                cbof.SelectedItem = 0;
                comboBox1.SelectedItem = 0;

            }
        }

        private void cbo1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClienteDTO cli = cbo1.SelectedItem as ClienteDTO;

            Teltext.Text = cli.Telefone;
            txtCep.Text = cli.CEP;
            txtEndereco.Text = cli.Endereço;
            txtnum.Text = cli.Numero;
            txtcomplemento.Text = cli.Complemento;
            txEmail.Text = cli.Email;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {



            try
            {
                if (radioButton1.Checked == true)
                {
                    ClienteEventualDTO dto = new ClienteEventualDTO();
                    dto.Nome = txtnome.Text;
                    dto.Endereço = txtEndereco.Text;
                    dto.Telefone = Teltext.Text;
                    dto.Numero = txtnum.Text;
                    dto.CEP = txtCep.Text;
                    dto.Complemento = txtcomplemento.Text;

                    ClienteEventualBusiness bus = new ClienteEventualBusiness();
                    bus.Salvar(dto);

                    EncomendaDTO enc = new EncomendaDTO();
                    enc.Data = DateTime.Now.Date;
                    enc.IdClienteEventual = dto.ID;
                    enc.Valor = Convert.ToDecimal(lbltotal.Text);
                    enc.Cliente = dto.Nome;
                    enc.Pagamento = cbof.SelectedItem.ToString();
                    enc.Entrega = comboBox1.SelectedItem.ToString();
                    enc.Status = "Pendente";

                    EncomendaBusiness b = new EncomendaBusiness();
                    b.Salvar(enc, produtosCarrinho.ToList());

                }
                if (radioButton2.Checked == true)
                {
                    ClienteDTO cliente = cbo1.SelectedItem as ClienteDTO;

                    EncomendaDTO enc = new EncomendaDTO();
                    enc.Data = DateTime.Now.Date;
                    enc.IdCliente = cliente.ID;
                    enc.Valor = Convert.ToDecimal(lbltotal.Text);
                    enc.Cliente = cliente.Nome;
                    enc.Pagamento = cbof.SelectedItem.ToString();
                    enc.Entrega = comboBox1.SelectedItem.ToString();
                    enc.Status = "Pendente";

                    EncomendaBusiness b = new EncomendaBusiness();
                    b.Salvar(enc, produtosCarrinho.ToList());



                    if (comboBox1.Text == "Delivery")
                    {
                        string nome = cbo1.Text;
                        string cep = txtCep.Text;
                        string rua = txtEndereco.Text;
                        string numero = txtnum.Text;
                        string valor = lbltotal.Text;
                        string conteudo = string.Empty;
                        string pagamento = cbof.Text;
                        string retirada = comboBox1.Text;



                        foreach (ProduçãoDTO item in produtosCarrinho)
                        {
                            conteudo = conteudo + " - " + item.Nome + " - " + item.Valor + "\n";

                        }
                        conteudo = "Olá, " + nome + " seu pedido ja foi finalizado.\n Ele foi solicitado para o endereço: \n \n CEP -" + cep + "\n \n Rua -" + rua + " \n \n Numero -" + numero + "\n \n E dentre de 15 - 30 minutos estará pronto \n \n Foi solicitado: \n" + conteudo + " No valor de: " + valor + "\n Forma de pagamento: " + pagamento + "\n Forma de retirada : " + retirada;


                        Email email = new Email();
                        email.Enviar(txEmail.Text, conteudo);
                    }
                    else 
                    {
                        string nome = cbo1.Text;
                        string cep = txtCep.Text;
                        string rua = txtEndereco.Text;
                        string numero = txtnum.Text;
                        string valor = lbltotal.Text;
                        string conteudo = string.Empty;
                        string pagamento = cbof.Text;
                        string retirada = comboBox1.Text;
                        conteudo = "Olá, " + nome + " seu pedido ja foi finalizado. /n /n Aguarde um instante"; 

                        Email email = new Email();
                        email.Enviar(txEmail.Text, conteudo);
                    }
                    

                }

                EnviarMensagem("Venda efetuada com sucesso");
                EnviarMensagemNovo("Deseja efetuar uma nova venda ?");
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Houve um erro ao efutar a compra: " + ex.Message);

                if (ex.Message.Contains("em falta"))
                {
                    DialogResult r = MessageBox.Show("Deseja ir ao estoque ?", "Panetteria Fiorentino",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        this.Hide();
                        Estoque a = new Estoque();
                        a.Show();
                    }
                }

                

            }
        }
           

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void txEmail_TextChanged(object sender, EventArgs e)
        {

        }

        private void PedidoVenda_Load_1(object sender, EventArgs e)
        {

        }

        private void dgvCarrinho_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex==0)
            {
                dgvCarrinho.Rows.RemoveAt(dgvCarrinho.CurrentRow.Index);
                ProduçãoDTO dto = cbo2.SelectedItem as ProduçãoDTO;
                decimal total = Convert.ToDecimal(lbltotal.Text);
                decimal valor = Convert.ToDecimal((total - dto.Valor));
                lbltotal.Text = valor.ToString();

            }
            
        }

       private void btnBuscarCEP_Click(object sender, EventArgs e)
        {
            string cep = txtCep.Text.Trim().Replace("-", "");

            CorreioResponseFunc correio = BuscarAPICorreio(cep);

          txtEndereco.Text = correio.Logradouro;
        }
        private CorreioResponseFunc BuscarAPICorreio(string cep)
        {

            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;


            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");


            CorreioResponseFunc correio = JsonConvert.DeserializeObject<CorreioResponseFunc>(resposta);
            return correio;
        
            }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public void Limpar()
        {
            for (int i = 0; i < dgvCarrinho.RowCount; i++)
            {
                dgvCarrinho.Rows[i].DataGridView.Rows.Clear();
            }
            cbo1.Visible = false;
            CarregarCombos();
            ConfigurarGrid();
            cbof.SelectedText = "Selecione";
            cbo1.Visible = false;
            lblemail.Visible = false;
            txEmail.Visible = false;
            Teltext.Visible = false;
            txtCep.Visible = false;
            txtEndereco.Visible = false;
            txtnum.Visible = false;
            txtcomplemento.Visible = false;
            lblc.Visible = false;
            lblce.Visible = false;
            lblco.Visible = false;
            lble.Visible = false;
            lblf.Visible = false;
            lbln.Visible = false;
            lblt.Visible = false;
            cbof.Visible = false;
            cbo1.Visible = false;
            txtnome.Visible = false;
            btnBuscarCEP.Visible = false;
            label2.Visible = false;
            comboBox1.Visible = false;

            radioButton1.Checked = false;
            radioButton2.Checked = false;

            txtquan.Text = "";
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> listar = business.Listar();
            cbof.SelectedItem = "Selecione";
            comboBox1.SelectedItem = "Selecione";
            txtCep.Text = "";
            Teltext.Text = "";
            txtEndereco.Text = "";
            txtcomplemento.Text = "";
            txtnum.Text = "";
            txEmail.Text = "";
            txtnome.Text = "";
            cbof.SelectedItem = 0;
            comboBox1.SelectedItem = 0;
            Teltext.Text = "";
            txtCep.Text = "";
            txtEndereco.Text = "";
            txtcomplemento.Text = "";
            txtnum.Text = "";
            txEmail.Text = "";
            txtnome.Text = "";

            comboBox1.SelectedItem = 0;
            cbo1.ValueMember = nameof(ClienteDTO.ID);
            cbo1.DisplayMember = nameof(ClienteDTO.Nome);
            cbo1.DataSource = listar;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private void label22_Click(object sender, EventArgs e)
        {

        }
    }
}

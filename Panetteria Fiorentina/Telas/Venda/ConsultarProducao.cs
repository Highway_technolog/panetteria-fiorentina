﻿using Panetteria_Fiorentina.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina.Telas.Venda
{
    public partial class ConsultarProducao : Form
    {
        public ConsultarProducao()
        {
            InitializeComponent();
            
        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }

        }
        public void CarregarGrid()
        {
           
            ProduçãoBusiness Business = new ProduçãoBusiness();
            List<ProduçãoDTO> lista = Business.Consultar();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 1)
                {
                    ProduçãoDTO producao = dataGridView1.CurrentRow.DataBoundItem as ProduçãoDTO;

                    DialogResult r = MessageBox.Show("Deseja excluir o item? Ao excluir o fornecedor, todos seus produtos e pedidos serão excluidos automaticamente.", "Panetteria Fiorentino",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        ProduçãoBusiness business = new ProduçãoBusiness();
                        /*ProdutoBusiness a = new ProdutoBusiness();
                        ItensPedidoBusiness b = new ItensPedidoBusiness();

                        b.Remover(fornecedor.Id);
                        a.Remover(fornecedor.Id);*/
                        business.Remover(producao.Id);

                        CarregarGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                EnviarMensagemErro(ex.Message);
            }
            if (e.ColumnIndex == 0)
            {
                ProduçãoDTO a = dataGridView1.CurrentRow.DataBoundItem as ProduçãoDTO;

                DialogResult r = MessageBox.Show("Deseja alterar o item?", "Panetteria Fiorentino",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ProduçãoDTO b = this.dataGridView1.Rows[e.RowIndex].DataBoundItem as ProduçãoDTO;
                    this.Close();
                    AlterarProducao tela = new AlterarProducao();
                    tela.LoadScrean(b);
                    tela.ShowDialog();
                }
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }
    }
    
}

﻿using Panetteria_Fiorentina.DB.Cliente;
using Panetteria_Fiorentina.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Panetteria_Fiorentina.Telas.Venda
{
    public partial class AlterarProducao : Form
    {
        public AlterarProducao()
        {
            InitializeComponent();
            
            CarregarCombo();
        }
        ProduçãoDTO dto;

        public void CarregarCombo()
        {
            List<string> tipo = new List<string>();
            tipo.Add("Selecione");
            tipo.Add("Revenda");
            tipo.Add("Produção");

            comboBox1.DataSource = tipo;
            comboBox1.SelectedItem = "Selecione";
        }
        public void LoadScrean(ProduçãoDTO dto)
        {
            this.dto = dto;
            txtnome.Text = dto.Nome;
            txtvalor.Text = dto.Valor.ToString();
            txtdes.Text = dto.Descricao;
            comboBox1.SelectedItem = dto.Tipo;
          

        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Panetteria Fiorentina",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void EnviarMensagemFechar()
        {
            DialogResult r = MessageBox.Show("Realmente deseja fechar o sistema ?", "Panetteria Fiorentina",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Warning);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
              
                this.dto.Nome = txtnome.Text;
                this.dto.Valor = Convert.ToDecimal(txtvalor.Text);
                this.dto.Descricao = txtdes.Text;
                this.dto.Tipo = comboBox1.SelectedItem.ToString();

    
               
                ProduçãoBusiness business = new ProduçãoBusiness();
                business.Alterar(dto);


                EnviarMensagem("Item do cardapio alterado com sucesso");
                ConsultarProducao tela = new ConsultarProducao();
                this.Close();
                tela.Show();
                tela.CarregarGrid();
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Houve erro ao alterar o item: " + ex.Message);
            }

        }

        private void label12_Click(object sender, EventArgs e)
        {
            EnviarMensagemFechar();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            ConsultarProducao tela = new ConsultarProducao();
            this.Close();
            tela.Show();
            tela.CarregarGrid();
        }

        private void AlterarProducao_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        { 
        }
    }
}
